<?php
include "header.php";
?>


<section id="slider" class="slider-parallax revslider-wrap clearfix">
    <div class="slider-parallax-inner">

        <!--
        #################################
            - THEMEPUNCH BANNER - SLIDER
        #################################
        -->
        <div class="tp-banner-container">
            <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
            <div id="rev_slider_1_1" class="rev_slider tp-banner fullscreenbanner" style="display:none;"
                 data-version="5.4.1">
                <ul>    <!-- SLIDE  -->
                    <li class="dark" data-index="rs-1" data-transition="fade" data-slotamount="default"
                        data-hideafterloop="0"
                        data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300"
                        data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2=""
                        data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8=""
                        data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="assets/images/slider/4be9e-BELIVEAU_F005p.jpg" alt="" data-bgposition="center center"
                             data-kenburns="on" data-duration="5000" data-ease="Linear.easeNone" data-scalestart="125"
                             data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-blurstart="0"
                             data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg"
                             data-no-retina>
                        <!-- LAYERS -->
                    </li>
                    <!-- SLIDE  -->
                    <li class="no-dark" data-index="rs-6" data-transition="fade" data-slotamount="default"
                        data-hideafterloop="0"
                        data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300"
                        data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2=""
                        data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8=""
                        data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="assets/images/slider/Goodluz.jpg" alt="" data-bgposition="center center"
                             data-kenburns="on" data-duration="10000" data-ease="Power2.easeIn" data-scalestart="125"
                             data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-blurstart="0"
                             data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg"
                             data-no-retina>
                        <!-- LAYERS -->
                    </li>
                    <!-- SLIDE  -->
                    <li class="dark" data-index="rs-4" data-transition="fade" data-slotamount="default"
                        data-hideafterloop="0"
                        data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300"
                        data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2=""
                        data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8=""
                        data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="assets/images/slider/bf1b9-BELIVEAU_G005p.jpg" alt="" data-bgposition="center center"
                             data-kenburns="on" data-duration="10000" data-ease="Power2.easeIn" data-scalestart="125"
                             data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-blurstart="0"
                             data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg"
                             data-no-retina>
                        <!-- LAYERS -->
                    </li>
                    <!-- SLIDE  -->
                    <li class="dark" data-index="rs-5" data-transition="fade" data-slotamount="default"
                        data-hideafterloop="0"
                        data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300"
                        data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2=""
                        data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8=""
                        data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="assets/images/slider/family-house.jpg" alt=""
                             data-bgposition="center center" data-kenburns="on" data-duration="10000"
                             data-ease="Power2.easeIn" data-scalestart="125" data-scaleend="100" data-rotatestart="0"
                             data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0"
                             data-offsetend="0 0" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                    </li>
                </ul>
               <!-- <div style="" class="tp-static-layers">

                    <!-- LAYER NR. 1 -->
                   <!-- <div class="tp-caption   tp-resizeme tp-static-layer"
                         id="slider-1-layer-7"
                         data-x="right" data-hoffset="-150px"
                         data-y="bottom" data-voffset="20px"
                         data-width="['335px']"
                         data-height="['525px']"
                         data-visibility="['on','on','off','off']"

                         data-type="text"
                         data-responsive_offset="on"

                         data-startslide="0"
                         data-endslide="3"
                         data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 6; min-width: 100%; max-width: 100%; max-width: 100%; max-width: 100%; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #000000;font-family:Open Sans;">
                        <div class="contact-widget" data-loader="button">-->

                            <!--
       #################################
           - FORM - SLIDER
       #################################
       -->
                          <!--  <div class="contact-form-result"></div>

                            <form id="contact-form-header "action="include/landing-5.php" method="post" role="form"
                                  class="landing-wide-form landing-form-overlay dark nobottommargin clearfix">
                                <div class="heading-block nobottommargin nobottomborder">
                                    <h3 class="center">Request Info</h3>
                                    <span></span>
                                </div>
                                <div class="line" style="margin: 20px 0 30px;"></div>
                                <div class="col_full form-group">
                                    <input type="text" name="template-landing5-name"
                                           class="form-control required input-lg not-dark" value=""
                                           placeholder="Your Name*">
                                </div>
                                <div class="col_full form-group">
                                    <input type="text" name="template-landing5-phone"
                                           class="form-control required input-lg not-dark" value=""
                                           placeholder="Your Phone Number*">
                                </div>
                                <div class="col_full form-group">
                                    <input type="email" name="template-landing5-email"
                                           class="form-control required input-lg not-dark" value=""
                                           placeholder="Your Email*">
                                </div>
                                <div class="col_full form-group">
                                    <label class="control-label " for="buyFrame">Buying Frame</label>
                                    <select class="selectpicker form-control  input-lg not-dark"
                                            name="template-landing5-buyFrame">
                                        <option>Now</option>
                                        <option>3 months</option>
                                        <option>6 months</option>
                                        <option>12 months or more</option>
                                    </select>
                                </div>
                                <div class="col_full form-group row">
                                    <div class="col-md-12">
                                        <label class="control-label " for="bedrooms">Number of Bedrooms:</label>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <input id="2bedrooms" class="radio-style" name="bedrooms" type="radio" checked>
                                        <label for="2bedrooms" class="radio-style-1-label radio-small">2
                                            Bedrooms</label>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <input id="3bedrooms" class="radio-style" name="bedrooms" type="radio">
                                        <label for="3bedrooms" class="radio-style-2-label radio-small">3
                                            Bedrooms</label>
                                    </div>
                                </div>
                                <div class="col_full row form-group">
                                    <div class="col-md-12">
                                        <label class="control-label " for="residence">Current Residence:</label>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <input id="vaudreuil" class="radio-style" name="residence" type="radio"
                                               checked>
                                        <label for="residence"
                                               class="radio-style-1-label radio-small">Vaudreuil</label>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <input id="westIsland" class="radio-style" name="residence" type="radio">
                                        <label for="residence" class="radio-style-2-label radio-small">West
                                            Island</label>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <input id="otherResidence" class="radio-style" name="residency"
                                               type="radio">
                                        <label for="residence"
                                               class="radio-style-2-label radio-small">Other</label>
                                    </div>

                                </div>

                                <div class="col_full form-group">
                            <textarea class="form-control  input-lg not-dark" id="message"
                                      name="template-landing5-message" cols="10" rows="2"
                                      placeholder="Your Message"></textarea>
                                </div>
                                <div class="col_full hidden form-group">
                                    <input type="text" id="template-landing5-botcheck" name="template-landing5-botcheck"
                                           value="" class="form-control"/>
                                </div>
                                <div class="col_full nobottommargin form-group">
                                    <button class="btn btn-lg btn-primary btn-block nomargin center " value="submit"
                                            type="submit" style="">SEND
                                    </button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>-->
                <div class="tp-bannertimer" style="height: 5px; background: rgba(122, 122, 122, 0.15);"></div>
            </div>-->
        </div><!-- END REVOLUTION SLIDER -->
    </div>

</section>



<!--
		#################################
			- TEXT ABOUT CONSTRUCTION LINQ
		#################################
		-->

<div id="about" class="section nomargin noborder" style="padding: 80px 0 0 0;">
    <div class="container center clearfix ">

        <div class="title-block title-center noborder">
            <h2>Urban Townhouse in <span>Châteaux de la Gare</span></h2>
            <h3>Vaudreuil - Dorion</h3>
        </div>

        <div class="col-md-12">
            <div class="feature-box fbox-rounded fbox-effect fbox-light text-feature">
                <p>Discover the LINQ townhouse project. Modern and refined, the living spaces harmonize colour accents
                    and create a coherence between materials and textures. Choosing an LINQ townhouse in Châteaux de la
                    Gare means taking full advantage of a lifestyle that balances design and modernity.
                    <br> <br>
                    These townhouses located in Vaudreuil-Dorion stand out with their intimate decor, stunning
                    exterior curb appeal, generous window space and abundant green spaces. Located only 5 minutes from
                    SmartCentres Vaudreuil II and at the junction of autoroutes 30, 40 and 20, the new residents will
                    also enjoy the proximity of the Vaudreuil-Dorion train station, a unique quality of life.
                    <br> <br>
                    Also notice the beautiful landscaping in the front and back of the buildings, a townhouse project
                    to discover!

                </p>
            </div>
        </div>

    </div>

</div>


<!--
		#################################
			- VIDEO
		#################################
		-->

<div id="imageBuilding" class="section nomargin notopborder  center vertical-center "
     style=" min-height: 650px;">
	<div class="container">

		<!--<img src="assets/videos/houseLinq.gif" alt="house gif">-->
		<video src="assets/videos/house.mp4" autoplay loop></video>
	</div>

</div>

		#################################
			- 3 Column DESCRIPTION
		#################################
		-->
<div id="descriptionConstruct" class="section nomargin noborder" style="padding: 0 0 80px 0;">
    <div class="container  clearfix ">

        <div class="title-block title-center noborder center">
            <h2>About <span>LINQ</span></h2>
            <span>Innovation and Construction</span>
        </div>

        <!--<div class="divider divider-short divider-rounded divider-center"><i class="icon-code"></i></div>-->

        <div class="col-md-4">
            <div class="feature-box fbox-rounded fbox-effect fbox-light">
                <div class="fbox-icon">
                    <a href="#"><i class="icon-foursquare i-alt"></i></a>
                </div>
                <h3>UNDERGROUND INFRASTRUCTURE</h3>
                <p>Underground cables and other amenity. Our attention to detail in everything that we do;
                    from our development locations which have been carefuly selected to our network of reliable
                    suppliers of thorough professionals, are what result in our consistent customer satisfaction.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="feature-box fbox-rounded fbox-effect fbox-light">
                <div class="fbox-icon">
                    <a href="#"><i class="icon-home i-alt"></i></a>
                </div>
                <h3>MODERN TOWNHOUSES</h3>
                <p>LINQ townhouses offers an inspired design that blends finesse and elegance.
                    Fluid, ergonomic and trend-infused spaces celebrate functionality and aesthetics.
                    The interiors benefit from high quality materials and upscale designs. The contemporary
                    architecture buildings are built around an interior courtyard and designed to offer an incomparable
                    quality living environment.
                </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="feature-box fbox-rounded fbox-effect fbox-light">
                <div class="fbox-icon">
                    <a href="#"><i class="icon-edit i-alt"></i></a>
                </div>
                <h3>CONTEMPORARY DESIGN</h3>
                <p>LINQ townhouses Design, visit our model unit and see for yourself the quality and aesthetics
                    of the project.
                </p>
            </div>
        </div>

    </div>
</div>

<!--
		#################################
			- Picture BUILDING ****** For now until we get plans picture see section bellow

		#################################
		-->

<div id="imageBuilding" class="section nomargin notopborder dark "
     style="background: url('assets/images/home/BELIVEAU_D005parallax.jpg'); background-size: cover;background-repeat: no-repeat; min-height: 700px;">


</div>
<!--

<!--
		#################################
			- PLANS   ****Hidden for now until we get plans that are approved by city....
		#################################
		-->
<div id="plans" class="section nomargin noborder bgcolor dark hidden" style="padding: 80px 0;">
    <div class="container center clearfix ">

        <div id="section-buy" class="heading-block title-center nobottomborder page-section">
            <h2>PLANS</h2>
        </div>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/BELIVEAU_D005p400.jpg" alt="townhouse 1">
                <div class="portfolio-overlay pictureImage pictureBox">
                    <a href="assets/images/home/BELIVEAU_D005p.jpg" class="center-icon" data-lightbox="image"><i
                                class="icon-line-plus"> </i></a>
                </div>
            </div>
            <div class="portfolio-desc">
                <h3>Towhouse 1</h3>
                <span>2000 pi</span>
            </div>
        </article>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/flr_lrR2240-1-400.jpg" alt="family yard">
                <div class="portfolio-overlay pictureImage pictureBox">
                    <a href="assets/images/home/flr_lrR2240-1.jpg" class="center-icon" data-lightbox="image"><i
                                class="icon-line-plus"> </i></a>
                </div>
            </div>
            <div class="portfolio-desc">
                <h3>Plan 1</h3>
                <span>2000 pi</span>
            </div>
        </article>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/BELIVEAU_G005p400.jpg">
                <div class="portfolio-overlay pictureImage pictureBox">
                    <a href="assets/images/home/BELIVEAU_G005p.jpg" class="center-icon" data-lightbox="image"><i
                                class="icon-line-plus"> </i></a>
                </div>
            </div>
            <div class="portfolio-desc">
                <h3>Towhouse 2</h3>
                <span>2000 pi</span>
            </div>
        </article>

	   <!-- <article class="portfolio-item pf-uielements pf-icons">
		    <div class="portfolio-image">
			    <img class="pictureImage pictureBox" src="assets/images/home/arial1.jpg" alt="townhouse 1">
			    <div class="portfolio-overlay pictureImage pictureBox">
				    <a href="assets/images/home/arial1.jpg" class="center-icon" data-lightbox="image"><i
							    class="icon-line-plus"> </i></a>
			    </div>
		    </div>
		    <div class="portfolio-desc">
			    <h3>Towhouse 1</h3>
			    <span>2000 pi</span>
		    </div>
	    </article>

	    <article class="portfolio-item pf-uielements pf-icons">
		    <div class="portfolio-image">
			    <img class="pictureImage pictureBox" src="assets/images/home/arial2.jpg" alt="family yard">
			    <div class="portfolio-overlay pictureImage pictureBox">
				    <a href="assets/images/home/arial2.jpg" class="center-icon" data-lightbox="image"><i
							    class="icon-line-plus"> </i></a>
			    </div>
		    </div>
		    <div class="portfolio-desc">
			    <h3>Plan 1</h3>
			    <span>2000 pi</span>
		    </div>
	    </article>

	    <article class="portfolio-item pf-uielements pf-icons">
		    <div class="portfolio-image">
			    <img class="pictureImage pictureBox" src="assets/images/home/arial3.jpg">
			    <div class="portfolio-overlay pictureImage pictureBox">
				    <a href="assets/images/home/arial3.jpg" class="center-icon" data-lightbox="image"><i
							    class="icon-line-plus"> </i></a>
			    </div>
		    </div>
		    <div class="portfolio-desc">
			    <h3>Towhouse 2</h3>
			    <span>2000 pi</span>
		    </div>
	    </article>-->
    </div>
</div>

<!--
		#################################
			- 3 Column Family NEIGHBORHOOD
		#################################
		-->
<div id="neighborhood" class="section nomargin noborder" style="padding: 80px 0;">
    <div class="container  clearfix ">

        <div class="title-block title-center noborder center">
            <h2>LIVING IN <span>VAUDREUIL-DORION</span></h2>

        </div>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/mall2_400.jpg" alt="mall">

            </div>
            <div class="portfolio-desc">

            </div>
        </article>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/bikepathWater400.jpg" alt="family yard">

            </div>
            <div class="portfolio-desc">

            </div>
        </article>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/gare2_400.jpg"

            </div>
            <div class="portfolio-desc">

            </div>
        </article>

        <div class="divider divider-short divider-rounded divider-center"><i class="icon-code"></i></div>

        <div class="col-md-12">
            <div class="feature-box fbox-rounded fbox-effect fbox-light text-feature">
                <p>Located in the west of Montreal, just a few minutes from downtown, Vaudreuil-Dorion offers a
                    complete and privileged living environment. This community benefits from a multitude of
                    recreational, cultural and community activities and facilities, provided for the wellbeing of all
                    its residents. The quality and the abundance of green spaces define this exceptional town – an
                    excellent environment for both living and working.
                    <br> <br>
                    Vaudreuil-Dorion, with a population of 30,000, offers you the quietude of the suburbs coupled with
                    the conveniences of the city, and all the excitement of a thriving community. Featuring a fast
                    growing number of new boutiques, restaurants, and services, as well as a multitude of tourist
                    attractions, cultural activities, sports and parks, your neighborhood has it all!
                </p>
            </div>
        </div>



    </div>
</div>

<!--
		#################################
			-  MAP
		#################################
		-->

<div id="imageMap" class="section nomargin notopborder dark"
     style="background: url('assets/images/home/lelinq_arial.jpg') center center; background-size: cover;background-repeat: no-repeat; min-height: 600px;">

</div>

<!--
		#################################
			- Gallery
		#################################
		-->
<div id="gallery" class="section nomargin noborder  " style="padding: 80px 0;">
    <div class="container center clearfix ">

        <div id="section-buy" class="heading-block title-center nobottomborder page-section">
            <h2>Gallery</h2>
        </div>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/bikepath400.jpg" alt="townhouse 1">
                <div class="portfolio-overlay pictureImage pictureBox">
                    <a href="assets/images/home/bikepath.jpg" class="center-icon" data-lightbox="image"><i
                                class="icon-line-plus"> </i></a>
                </div>
            </div>
            <div class="portfolio-desc">

            </div>
        </article>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/gare400.jpg">
                <div class="portfolio-overlay pictureImage pictureBox">
                    <a href="assets/images/home/gare.jpg" class="center-icon" data-lightbox="image"><i
                                class="icon-line-plus"> </i></a>
                </div>
            </div>
            <div class="portfolio-desc">

            </div>
        </article>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/lecon-golf400.jpg" alt="family yard">
                <div class="portfolio-overlay pictureImage pictureBox">
                    <a href="assets/images/home/lecon-golf.jpg" class="center-icon" data-lightbox="image"><i
                                class="icon-line-plus"> </i></a>
                </div>
            </div>
            <div class="portfolio-desc">

            </div>
        </article>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/mall400.jpg">
                <div class="portfolio-overlay pictureImage pictureBox">
                    <a href="assets/images/home/mall.jpg" class="center-icon" data-lightbox="image"><i
                                class="icon-line-plus"> </i></a>
                </div>
            </div>
            <div class="portfolio-desc">

            </div>
        </article>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/mall3_400.jpg" alt="family yard">
                <div class="portfolio-overlay pictureImage pictureBox">
                    <a href="assets/images/home/mall3.jpg" class="center-icon" data-lightbox="image"><i
                                class="icon-line-plus"> </i></a>
                </div>
            </div>
            <div class="portfolio-desc">

            </div>
        </article>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/mall4_400.jpg">
                <div class="portfolio-overlay pictureImage pictureBox">
                    <a href="assets/images/home/mall4.jpg" class="center-icon" data-lightbox="image"><i
                                class="icon-line-plus"> </i></a>
                </div>
            </div>
            <div class="portfolio-desc">

            </div>
        </article>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/mall7_400.jpg" alt="family yard">
                <div class="portfolio-overlay pictureImage pictureBox">
                    <a href="assets/images/home/mall7.jpg" class="center-icon" data-lightbox="image"><i
                                class="icon-line-plus"> </i></a>
                </div>
            </div>
            <div class="portfolio-desc">

            </div>
        </article>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/tenniscourt400.jpg" alt="family yard">
                <div class="portfolio-overlay pictureImage pictureBox">
                    <a href="assets/images/home/tenniscourt.jpg" class="center-icon" data-lightbox="image"><i
                                class="icon-line-plus"> </i></a>
                </div>
            </div>
            <div class="portfolio-desc">

            </div>
        </article>

        <article class="portfolio-item pf-uielements pf-icons">
            <div class="portfolio-image">
                <img class="pictureImage pictureBox" src="assets/images/home/watersight400.jpg">
                <div class="portfolio-overlay pictureImage pictureBox">
                    <a href="assets/images/home/watersight.jpg" class="center-icon" data-lightbox="image"><i
                                class="icon-line-plus"> </i></a>
                </div>
            </div>
            <div class="portfolio-desc">

            </div>
        </article>

	    <article class="portfolio-item pf-uielements pf-icons">
		    <div class="portfolio-image">
			    <img class="pictureImage pictureBox" src="assets/images/home/arial1.jpg" alt="family yard">
			    <div class="portfolio-overlay pictureImage pictureBox">
				    <a href="assets/images/home/arial1.jpg" class="center-icon" data-lightbox="image"><i
							    class="icon-line-plus"> </i></a>
			    </div>
		    </div>
		    <div class="portfolio-desc">

		    </div>
	    </article>

	    <article class="portfolio-item pf-uielements pf-icons">
		    <div class="portfolio-image">
			    <img class="pictureImage pictureBox" src="assets/images/home/arial2.jpg" alt="family yard">
			    <div class="portfolio-overlay pictureImage pictureBox">
				    <a href="assets/images/home/arial2.jpg" class="center-icon" data-lightbox="image"><i
							    class="icon-line-plus"> </i></a>
			    </div>
		    </div>
		    <div class="portfolio-desc">

		    </div>
	    </article>

	    <article class="portfolio-item pf-uielements pf-icons">
		    <div class="portfolio-image">
			    <img class="pictureImage pictureBox" src="assets/images/home/arial3.jpg">
			    <div class="portfolio-overlay pictureImage pictureBox">
				    <a href="assets/images/home/arial3.jpg" class="center-icon" data-lightbox="image"><i
							    class="icon-line-plus"> </i></a>
			    </div>
		    </div>
		    <div class="portfolio-desc">

		    </div>
	    </article>
    </div>
</div>



</div>

#################################
- CONTACT REQUEST INFO
#################################
-->
<div id="request" class="section nomargin notopborder">

    <div class="container clearfix dark">
        <div class="divider  divider-rounded divider-center"><i class="icon-code"></i></div>


        <div id="section-buy" class="heading-block title-center nobottomborder page-section">
            <h2>REQUEST <span style="color: #8b8b8b;">INFO</span></h2>
            <span>info@lelinq.com</span>
        </div>

        <div>

            <form id="contact-form" class="row" method="post" action="#" novalidate style="color: #8b8b8b;">
                <div class="form-group col-md-6 col-md-offset-3">
                    <label class="control-label requiredField" for="name_form">Name
                        <span class="asteriskField">*</span>
                    </label>
                    <input class="form-control" id="name" name="name"/>
                </div>
                <div class="form-group col-md-6 col-md-offset-3">
                    <label class="control-label requiredField" for="phone_form">
                        Phone<span class="asteriskField">*</span>
                    </label>
                    <input class="form-control" id="phone" name="phone" type="tel"/>
                </div>
                <div class="form-group col-md-6 col-md-offset-3">
                    <label class="control-label requiredField" for="email_form">
                        Email<span class="asteriskField">*</span>
                    </label>
                    <input class="form-control" id="email" name="email" type="email"/>
                </div>
                <div class="form-group col-md-6 col-md-offset-3">
                    <label class="control-label " for="buyFrame_form">
                        When are you looking to buy:<span class="asteriskField"></span>
                    </label>
                    <select class="selectpicker form-control   not-dark"
                            name="buying_frame">
                        <option value="now">Now</option>
                        <option value="3 months">3 months</option>
                        <option value="6 months">6 months</option>
                        <option value="12 months+">12 months or more</option>
                    </select>
                </div>

                <div class="form-group col-md-6 col-md-offset-3">
                    <div class="col-md-12">
                        <label class="control-label requiredField" for="bedrooms">
                            Number of Bedrooms:<span class="asteriskField"></span>
                        </label>
                    </div>

                    <div class="col-md-5">
                        <input id="2bedrooms" class=" radio-style" name="bedrooms" value="2 bedrooms" type="radio"
                               checked> <span></span>
                        <label for="2bedrooms" class="radio-style-1-label radio-small">2
                            Bedrooms</label>
                    </div>
                    <div class="col-md-4">
                        <input id="3bedrooms" class="  radio-style" name="bedrooms" value="3 bedrooms" type="radio"><span></span>
                        <label for="3bedrooms" class="radio-style-2-label radio-small">3
                            Bedrooms</label>
                    </div>
                </div>

                <div class="form-group col-md-6 col-md-offset-3">
                    <div class="col-md-12">
                        <label class="control-label requiredField" for="residence">
                            Current Residence: <span class="asteriskField"></span>
                        </label>
                    </div>
                    <div class="col-md-5">
                        <input id="vaudreuil_form" class=" radio-style" name="residence" value="Vaudreuil" type="radio"
                               checked><span></span>
                        <label for="vaudreuil_form"
                               class="radio-style-1-label radio-small">Vaudreuil-Dorion</label>
                    </div>
                    <div class="col-md-4">
                        <input id="westIsland_form" class=" radio-style" name="residence" value="West Island"
                               type="radio"><span></span>
                        <label for="westIsland_form" class="radio-style-2-label radio-small">West
                            Island</label>
                    </div>
                    <div class="col-md-3">
                        <input id="otherResidence_form" class="radio-style" value="Other" name="residence"
                               type="radio"><span></span>
                        <label for="otherResidence_form"
                               class="radio-style-2-label radio-small">Other</label>
                    </div>
                </div>
                <div class="form-group col-md-6 col-md-offset-3">
                    <label class="control-label " for="message">
                        Message
                    </label>
                    <textarea class="form-control" id="message" name="message" cols="10" rows="10"></textarea>

                </div>
                <div class="form-group col-md-12 center">
                    <div>
                        <button class="btn btn-submit btn-lg noradius font-primary" name="submit">
                            <i class="icon-paperplane"></i> SEND
                        </button>
                    </div>
                </div>
            </form>

            <h3 id="contact-form-message" class="center"></h3>

        </div>

    </div>

</div>


<?php
include "footer.php";
?>
