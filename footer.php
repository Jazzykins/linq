</div><!-- #wrapper end -->

<!-- Footer
		============================================= -->
<footer id="footer" class="dark">



        <!-- Copyrights
        ============================================= -->
        <div id="copyrights">

            <div class="container clearfix">

                <div class="center whiteText">
                    Copyrights &copy; <?php echo date("Y"); ?> All Rights Reserved by Le LINQ.
                    <br>
                    deisgn by W3Consultation
                </div>



            </div>

        </div><!-- #copyrights end -->

</footer><!-- #footer end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="assets/js/jquery.vmap.js"></script>
<script type="text/javascript" src="assets/js/vmap/jquery.vmap.world.js"></script>
<script type="text/javascript" src="assets/js/vmap/jquery.vmap.usa.js"></script>
<script type="text/javascript" src="assets/js/vmap/jquery.vmap.europe.js"></script>
<script type="text/javascript" src="assets/js/vmap/jquery.vmap.germany.js"></script>
<script type="text/javascript" src="assets/js/vmap/jquery.vmap.sampledata.js"></script>
<script type="text/javascript"
        src="https://maps.google.com/maps/api/js?key=AIzaSyDMxJ92oBkSnVNHFX3R8XhtYQPEgk1_IiI"></script>
<script type="text/javascript" src="assets/js/jquery.gmap.js"></script>
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/plugins.js"></script>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="assets/js/functions.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>

<!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
<script type="text/javascript" src="assets/include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="assets/include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="assets/include/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript"
        src="assets/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript"
        src="assets/include/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript"
        src="assets/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript"
        src="assets/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript"
        src="assets/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript"
        src="assets/include/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript"
        src="assets/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>

</body>
</html>