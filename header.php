<?php

?>

<!DOCTYPE html>
<html lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<meta name="author" content="SemiColonWeb"/>

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:100,300,400,500,600,700|Crete+Round:400italic"
	      rel="stylesheet" type="text/css"/>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css"/>
	<link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
	<link rel="stylesheet" href="assets/css/dark.css" type="text/css"/>
	<link rel="stylesheet" href="assets/css/font-icons.css" type="text/css"/>
	<link rel="stylesheet" href="assets/css/animate.css" type="text/css"/>
	<link rel="stylesheet" href="assets/css/magnific-popup.css" type="text/css"/>
	<link rel="stylesheet" href="assets/css/jquery.orgchart.css" type="text/css"/>

	<link rel="stylesheet" href="assets/css/responsive.css" type="text/css"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="assets/include/rs-plugin/css/settings.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="assets/include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="assets/include/rs-plugin/css/navigation.css">

	<!-- Custom css and overrides -->
	<link rel="stylesheet" href="assets/css/custom.css" type="text/css"/>

	<!-- jQuery -->
	<script type="text/javascript" src="assets/js/jquery.js"></script>

	<!-- Document Title
	============================================= -->
	<title>Linq</title>
    <link rel="shortcut icon" href="assets/images/icons/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="assets/images/icons/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/icons/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/icons/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/icons/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/icons/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/icons/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/icons/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/icons/apple-touch-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/icons/apple-touch-icon-180x180.png" />
</head>


<body class="stretched">
<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

	<!-- Header
	============================================= -->
	<header id="header" class="transparent-header semi-transparent" data-sticky-class="not-dark">

		<div id="header-wrap">

			<div class="container clearfix">

				<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

				<!-- Logo
				============================================= -->
				<div id="logo">
					<a href="#" data-href="#slider" class="standard-logo"
					   data-dark-logo="assets/images/linq300.png"
                       data-sticky-logo="assets/images/linq150a.png">
						<img src="assets/images/linq300.png" alt="Linq Logo">
					</a>
					<a href="#" data-href="#slider" class="retina-logo"
					   data-dark-logo="assets/images/linq300.png"
                       data-sticky-logo="assets/images/linq150a.png">
						<img src="assets/images/linq150.png" alt="Linq Logo">
					</a>
				</div><!-- #logo end -->

				<!-- Primary Navigation
				============================================= -->
				<nav id="primary-menu" class="style-2 style-6  pull-right">

					<ul class="one-page-menu" data-offset="50">
						<li class="current">
							<a href="#" data-href="#slider">
								<div>Home</div>
							</a>
						</li>
						<li>
							<a href="#" data-href="#plans">
								<div>Plans</div>
							</a>
						</li>
                        <li>
                            <a href="#" data-href="#neighborhood">
                                <div>Neighborhood</div>
                            </a>
                        </li>
						<li>
							<a href="#" data-href="#gallery">
								<div>Gallery</div>
							</a>
						</li>
                        <li>
                            <a href="#" data-href="#request">
                                <div>Request</div>
                            </a>
                        </li>

					</ul>

					<!-- Top Search
					============================================= -->
					<!--<div id="top-search">
						<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
						<form action="/" method="get">
							<input type="text" name="q" class="form-control" value=""
							       placeholder="Type &amp; Hit Enter..">
						</form>
					</div>--><!-- #top-search end -->

				</nav><!-- #primary-menu end -->

			</div>

		</div>

	</header><!-- #header end -->