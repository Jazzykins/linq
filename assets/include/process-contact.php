<?php

include __DIR__ . '/phpmailer/PHPMailerAutoload.php';

spl_autoload_register(function ($class) {
    require_once("../../app/models/$class.class.php");
});

/*$addEmailActionToken = Tokenizer::add('post-action-email', 20, 'email');
$addEmailCaseToken = Tokenizer::add('post-case-add', 30, 'add');*/

$db = new RegistrationDBManager();

$rules = array(
    'name' => 'required|max_len,150|min_len,4',
    'phone' => 'required|max_len,150|min_len,4',
    'email' => 'required|max_len,150|min_len,4',
    'buying_frame' => 'required',
    'bedrooms' => 'required',
    'residence' => 'required',
    'message' => 'max_len,500');

$filters = array(
    'name' => 'trim|sanitize_string',
    'phone' => 'trim|sanitize_string',
    'email' => 'trim|sanitize_string',
    'buying_frame' => 'trim|sanitize_string',
    'bedrooms' => 'trim|sanitize_string',
    'residence' => 'trim|sanitize_string',
    'message' => 'trim|sanitize_string');

$fileTypes = array(
	"jpeg", "jpg", "png", "gif", "pdf", "docx", "txt", "rtf", "doc"
);

if(Functions::validateArray($_POST, $rules, $filters, $error)) {
    if ($db->addRegistration($_POST)) {
        echo '{ "alert": "success", "message": "Thank you! We will contact you shortly!" }';
    } else {
        if (Config::DEBUG_CORE)
            echo Config::DEFAULT_DB_ERROR;
        else{
            echo '{ "alert": "error", "message": "Email <strong>could not</strong> be sent due to some Unexpected Error. Please Try Again later." }';
        }
    }
   /* $to = "aymandiab013@gmail.com";
		$from = $_POST['email'];
		$name = $_POST['name'];
		$agentID = $_POST['agentID'];
		$phone = $_POST['phone'];
		$headers = "From: $from";
		$subject = "Connectimo form";

		$fields = array();
		$fields{"name"} = "Nom";
		$fields{"email"} = "Email";
		$fields{"phone"} = "Phone";
		$fields{"agentID"} = "Agent ID";

		$body = "";
		foreach ($fields as $a => $b) {
			$body .= sprintf("%20s: %s <br>", $b, $_POST[$a]);
		}

		if($error = Functions::sendEmail($from, $to, $subject, $body)) {
			echo '{ "alert": "success", "message": "Email Sent!" }';
		}else
			echo '{ "alert": "error", "message": "Email <strong>could not</strong> be sent due to some Unexpected Error. Please Try Again later." }';*/
}else{
	echo '{ "alert": "error", "message": "Please fill the required fields!" }';
	//echo '{ "alert": "error", "message": "'.str_replace('"', '\"',  $error).'" }';
}
?>