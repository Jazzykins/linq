
$(function () {
	var apiRevoSlider = $('.tp-banner').show().revolution({
			sliderType: "standard",
			jsFileLocation: "include/rs-plugin/js/",
			dottedOverlay: "none",
			delay: 16000,
			startwidth: 1140,
			startheight: 600,
			hideThumbs: 200,

			thumbWidth: 100,
			thumbHeight: 50,
			thumbAmount: 5,

			navigation: {
				keyboardNavigation: "on",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				},
				arrows: {
					style: "hades",
					enable: true,
					hide_onmobile: false,
					hide_onleave: false,
					tmp: '<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div></div>',
					left: {
						h_align: "left",
						v_align: "center",
						h_offset: 10,
						v_offset: 0
					},
					right: {
						h_align: "right",
						v_align: "center",
						h_offset: 10,
						v_offset: 0
					}
				},
			},

			touchenabled: "on",
			onHoverStop: "on",

			swipe_velocity: 0.7,
			swipe_min_touches: 1,
			swipe_max_touches: 1,
			drag_block_vertical: false,

			parallax: "mouse",
			parallaxBgFreeze: "on",
			parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],

			keyboardNavigation: "off",

			navigationHAlign: "center",
			navigationVAlign: "bottom",
			navigationHOffset: 0,
			navigationVOffset: 20,

			soloArrowLeftHalign: "left",
			soloArrowLeftValign: "center",
			soloArrowLeftHOffset: 20,
			soloArrowLeftVOffset: 0,

			soloArrowRightHalign: "right",
			soloArrowRightValign: "center",
			soloArrowRightHOffset: 20,
			soloArrowRightVOffset: 0,

			shadow: 0,
			fullWidth: "off",
			fullScreen: "on",

			spinner: "spinner4",

			stopLoop: "off",
			stopAfterLoops: -1,
			stopAtSlide: -1,

			shuffle: "off",

			autoHeight: "off",
			forceFullWidth: "off",


			hideThumbsOnMobile: "off",
			hideNavDelayOnMobile: 1500,
			hideBulletsOnMobile: "off",
			hideArrowsOnMobile: "off",
			hideThumbsUnderResolution: 0,

			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			startWithSlide: 0,
		});

	apiRevoSlider.bind("revolution.slide.onloaded", function (e) {
		setTimeout(function () {
			SEMICOLON.slider.sliderParallaxDimensions();
		}, 200);
	});

	apiRevoSlider.bind("revolution.slide.onchange", function (e, data) {
		SEMICOLON.slider.revolutionSliderMenu();
	});

	$("#contact-form").submit(function (e) {
		e.preventDefault();
		var form = this,
		messageEl = $("#contact-form-message");
		$(form).find('[type="submit"]').html('<i class="fa fa-spinner fa-spin"></i> Sending');
		$.ajax({
			url: 'assets/include/process-contact.php',
			data: new FormData(form),
			type: 'POST',
			contentType: false,
			processData: false,
			success: function (data) {
				//console.log(data);
				data = JSON.parse(data);
				if(data.alert === 'success') {
					$(form).html('<h3 class="center">' + data.message + '</h3>');
					messageEl.empty();
				}else{
					$(form).find('[type="submit"]').html('<i class="icon-paperplane"></i> SEND');
					messageEl.html(data.message);
				}
			}
		});
	});

    /*$('#map').gMap({
        address: 'Château de la Gare',
        maptype: 'ROADMAP',
        zoom: 13,
        markers: [
            {
                address: "Château de la Gare, Vaudreuil-Dorion, QC",
                html: "Château de la Gare, Vaudreuil-Dorion, QC"
            },
            {
                address: "Château de la Gare, Vaudreuil-Dorion, QC",
                html: "Château de la Gare, Vaudreuil-Dorion, QC"
            },
            {
                address: "Château de la Gare, Vaudreuil-Dorion, QC",
                html: "Château de la Gare, Vaudreuil-Dorion, QC"
            }
        ],
        doubleclickzoom: false,
        controls: {
            panControl: true,
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: false
        },
		styles: [
            {
                "featureType": "all",
                "stylers": [
                    {
                        "saturation": 0
                    },
                    {
                        "hue": "#404b8c"
                    }
                ]
            },
            {
                "featureType": "road",
                "stylers": [
                    {
                        "saturation": -70
                    }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "stylers": [
                    {
                        "visibility": "simplified"
                    },
                    {
                        "saturation": -60
                    }
                ]
            }
        ]
    });*/

});

