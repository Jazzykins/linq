<?php require_once('header.php'); ?>
<?php require_once('load/menu-top.php'); ?>
<?php require_once('load/menu-left.php'); ?>
<?php
Functions::redirect("registrations");
$dynamicFormId = Tokenizer::get('dynamic-form-id', Tokenizer::GET_TOKEN);
?>

<div class="wrapper">
    <div class="container">

    	<div class="row">
    		<div class="col-md-12 col-sm-12 col-xs-12">
    			<div class="x_panel tile">
    				<div class="x_title">
    					<h2>Dashboard</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
              </ul>
    					<div class="clearfix"></div>
    				</div>
    				<div class="x_content">
    					<div class="btn-group pull-right m-t-15">
    						<a href="#" class="btn btn-default waves-effect waves-light troll">Settings <span class="m-l-5"><i class="fa fa-cog"></i></a>
    					</div>
    					<p class="text-muted page-title-alt">Welcome back <?php echo $_SESSION['user']['name']; ?>!</p>
    				</div>
    			</div>
    		</div>
    	</div>

	</div>
</div>

<?php require_once('footer.php'); ?>
<?php require_once('foot.php'); ?>