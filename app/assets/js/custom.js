$(function () {
	//tooltips
	$("[title]").tooltip();

	//flipclock
	var clock = $('.clock').FlipClock({
		clockFace: 'TwelveHourClock'
	});
});

/**
 * Function to convert any string into a SLUG.
 * @param  string to convert
 * @return the string in a sluggy format.
 */
function slugifyString(string) {
  return string.toString().toLowerCase()
	.replace(/\s+/g, '-')           // Replace spaces with -
	.replace(/[^\w\-]+/g, '')       // Remove all non-word chars
	.replace(/\-\-+/g, '-')         // Replace multiple - with single -
	.replace(/^-+/, '')             // Trim - from start of text
	.replace(/-+$/, '');            // Trim - from end of text
}
