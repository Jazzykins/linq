var lastToast = null;
var removeDisable;
var invoiceCounter = 0;

$(document).ready(function(){
    // Message for upcoming links
    $('body').on('click', 'a.soon[href="#"]', function(e){
        e.preventDefault();
        generateNotification("That's too bad!! [Coming soon...]", "bottom-right", "info", 5000);
    });

    //after-success
    if (!!$.cookie('success-message')) {
        toastr.success($.cookie('success-message'), {timeOut: 3000});
        $.removeCookie("success-message");
        //generateNotification($.cookie('success-message'), 'bottom-right', 'success');
    }

    //after-error
    if (!!$.cookie('error-message')) {
        toastr.error($.cookie('error-message'), {timeOut: 3000});
        $.removeCookie("error-message");
        //generateNotification($.cookie('error-message'), 'bottom-right', 'error');
    }

    //On form submission
    $("body").on('submit', "form[data-parsley-validate='']", function(){
        if($(this).parsley().validate())
        return validate(this);

        return false;
    });

});

/**
*   Function to clear the last toast.
*/
function clearToast(){
    if(lastToast != null)
    toastr.clear(lastToast);
}
/**
*	Function to do a list of actions after an ajax call.
*	action:				Name of the callback action.
*	data:				Data needed for the action as value or associative array.
*	Last updated:		2015-10-13
*/
function executeAction(action, data){
    switch(action){
        case "add-user":
        case "delete-user":
        case "edit-user":
            hideModal();
            $('div#users-wrapper').load('load/users.php');
            break;
        case 'reload-registrations':
            hideModal();
            $('div#registrations-wrapper').load('load/registrations.php');
            break;
	    /*case 'reload-emails':
		    hideModal();
		    $('div#registrations-wrapper').load('load/registrations.php');
		    break;*/

        case 'get-single-registration':
            modal = $('#view-modal');
            modal.find('[data-name]').html(data['registration_name']);
            modal.find('[data-phone]').html(data['registration_phone']);
            modal.find('[data-email]').html(data['registration_email']);
            modal.find('[data-buying_frame]').html(data['registration_buying_frame']);
            modal.find('[data-bedrooms]').html(data['registration_bedrooms']);
            modal.find('[data-residence]').html(data['registration_residence']);
            modal.find('[data-message]').html(data['registration_message']);
            break;
        case 'get-single-edit-registration':
            modal = $('#registration-modal');
            modal.find('#id').val(data['registration_id']);
            modal.find('#name').val(data['registration_name']);
            modal.find('#phone').val(data['registration_phone']);
            modal.find('#email').val(data['registration_email']);
            modal.find('#buying_frame').val(data['registration_buying_frame']);
            modal.find('[value="'+data['registration_bedrooms']+'"]').attr('checked', true);
            modal.find('[value="'+data['registration_residence']+'"]').attr('checked', true);
            modal.find('#message').val(data['registration_message']);
            break;
	    case 'get-single-email':
		    modal = $('#email-view-modal');
		    modal.find('[data-subject]').html(data['email_subject']);
		    modal.find('[data-message]').html(data['email_message']);
		    break;
        case 'get-single-send-email':
		    $('.blank-email-container').hide();
		    $('.email-container').show(300);
		    $('span#email-title').html('<div style="font-size: 75%; font-weight: bold; padding-bottom: 8px;">Subject:</div>' + data['email_subject']);
		    $('div.email-body').html(data['email_message']);
		    break;
        case 'blank-send-email':
	        $('.email-container').hide();
	        $('.blank-email-container').show(300);
            break;
	    case 'get-single-edit-email':
		    modal = $('#email-modal');
		    modal.find('#id').val(data['email_id']);
		    modal.find('#email_subject').val(data['email_subject']);
		    modal.find('#email_message').summernote('code', data['email_message']);
		    break;
        case 'debug':
            console.log(data);
            break;
        case "swal":
            data = $.type(data) != "undefined" ? data : "";
            var message = $.type(data.message) != "undefined" ? data.message : " ";
            var type = $.type(data.type) != "undefined" ? data.type : "info";
            var title = $.type(data.title) != "undefined" ? data.title : capitalize(type);
            swal(title, message, type);

            if($.type(data['next-action']) != "undefined")
            executeAction(data['next-action'], $.type(data['next-action-data']) != "undefined" ? data['next-action-data'] : "");
            break;
        default:
            generateNotification('Unknown callback action: ' + action, 'bottom-right', 'warning', 3000, false);
    }
}

/**
*	Function to generate a notification.
*	@message:			Message of the notification.
*	@layout:			Default: top-full-width (top-full-width | top-left | top-center | top-right | bottom-left | bottom-center | bottom-right | bottom-full-width)
*	@type:				Default: warning (success | info | warning | error)
*	@duration:			Duration of the notification in miliseconds. Default: 30000 (30 seconds)
*   @isKiller:          True if the last toast will be cleared.
*   @hasCloseButton:    True to show the close button. Default: false
*   @callback:          Name of the callback action for the function executeAction.
*	Last updated: 		2015-12-28
*/
function generateNotification(message, layout, type, duration, isKiller, hasCloseButton, callback){
    // List of possible layouts and types
    var layouts = ["top-full-width", "top-left", "top-center", "top-right", "bottom-left", "bottom-center", "bottom-right", "bottom-full-width"];
    var types = ["success", "info", "warning", "error"];

    // Check if the layout exist. If it doesn't, set it to top.
    if($.type(layout) !== "string" || $.inArray(layout, layouts) == -1)
    layout = "top-full-width";

    // Check if the type exist. If it doesn't, set it to warning.
    if($.type(type) !== "string" || $.inArray(type, types) == -1)
    type = "warning";

    // If duration is not set, set it to 30 seconds.
    if($.type(duration) !== "number" || duration < 0)
    duration = 30000;

    // Set killer to true.
    if(($.type(isKiller) !== "boolean" && lastToast != null) || isKiller === true)
    clearToast();

    // Set the close button to false
    if($.type(hasCloseButton) !== "boolean" || hasCloseButton === false)
    hasCloseButton = false;

    // Set the toast options
    toastr.options = {
        "closeButton" : hasCloseButton,
        "positionClass" : "toast-" + layout,
        "timeOut" : duration,
        "preventDuplicates": true,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "showDuration" : 300,
        "hideDuration" : 300
    };

    // Set the callback if there's one
    if($.type(callback) === "string")
    toastr.options.onclick = function(){ executeAction(callback); };

    // Display the toast and save it as the last toast
    lastToast = toastr[type](message);
}

/*
*	Function to hide the bootstrap modal.
*/
function hideModal(){
    $('.modal').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}

/**
*	Function to validate a specific form via ajax.
*	@pForm:				The form to validate.
*	@pContinue:			If true, action will continue. Default: false.
*	@pRemoveDisable:	True to disable all form inputs during the form submission. Default: true
*	Last updated: 		2016-02-01
*/
function validate(pForm, pContinue, pDisable){
    // Check if pContinue is set and has a valid value. If not, set it to false.
    if($.type(pContinue) !== "boolean")
    pContinue = false;

    // Check if pDisable is set and has a valid value. If not, set it to true
    if($.type(pDisable) !== "boolean")
    pDisable = true;

    // Send the request to the core
    $.ajax({
        url: 'core.php',
        type: 'POST',
        data: new FormData(pForm),
        dataType: 'json',
        contentType: false,
        cache: false,
        processData:false,
        beforeSend: function(){
            // Close all notifications
            toastr.clear();

            // Add the readonly attribute to each input and disable the buttons
            if(pDisable){
                $('input[type="submit"], button[type="submit"], input').attr('disabled', true);
                $('form input, form select').attr('readonly', 'readonly');
                removeDisable = true;
            }

            // Show the ajax loader
            $('div.loader-holder').fadeIn();
        },
        success: function(data){
            if(data['error'] != -1)
            generateNotification(data['error'], data['layout'], 'error', 3000, true, data['toast-button'], data['toast-callback']);
            else{
                // Display the message unless specified not to
                if($.type(data['no-message']) !== "boolean" || data['no-message'] == false)
                generateNotification(data['success'], data['layout'], 'success', data['duration'], data['killer'], data['toast-button'], data['toast-callback']);
            }

            // Check if specified to not clear form
            if($.type(data['reset']) === "boolean" && data['reset'] == true)
            $(pForm).trigger('reset');

            // Set the remove-disable property
            removeDisable = data['remove-disable'];

            // Check if there's a redirect
            if($.type(data['redirect']) === "boolean" && data['redirect'] == true)
            setTimeout(function(){
                if($.type(data['refresh']) === "boolean" && data['refresh'] == true)
                window.location.reload();
                else
                window.location.href = data['destination'];
            }, data['delay']);

            // Check if there's a callback
            if(data['callback'] != null && data['callback'] != "undefined")
            executeAction(data['callback'], data['callback-data']);
        },
        error: function(xhr, textStatus, errorThrown){
            generateNotification('Response error: ' + xhr.responseText, 'top-full-width', 'error', 30000);
        },
        complete: function(){
            // Hide the ajax loader
            $('div.loader-holder').fadeOut();

            // Remove the readonly/disabled attribute from inputs and buttons
            if(removeDisable){
                $('input[type="submit"], button[type="submit"], input').removeAttr('disabled');
                $('form input, form select').removeAttr('readonly');
            }
        }
    });

    return pContinue;
}

/**
*	Function to update the url without refreshing
*	@url:				The extension to add to after the website url without trailing slash
*/
function updateUrl(url){
    history.pushState('data', '', window.location.protocol + "//" + window.location.hostname + url);
}

/*
*	Function to generate a random number.
*	@min: minimum inclusive.
*	@max: maximum inclusive.
*/
function generateRandomNumber(min, max){
    if(isNaN(min))
    min = 1;

    if(isNaN(max))
    max = 54;

    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/*
*	Function to generate a random string.
*	@length: length of the string. Default: 20
*	@hasNumbers: true if the string can contain digits. Default: true
*	@intBegin: true if the string can start with a number. Default: false
*	@hasSymbols: true if the string can contain symbols. Default: false
*	@symbolBegin: true if the string can begin with a symbol. Default: false
*/
function generateRandomString(length, hasNumbers, intBegin, hasSymbols, symbolBegin){
    var characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var digits = "012345679";
    var symbols = "!@#$%^&*()-_+=";
    var result = "";
    var invalidStart = "";

    // Set the default length to 20
    if(isNaN(length))
    length = 20;

    if($.type(hasNumbers) !== "boolean" && hasNumbers === true){
        // Set the default intBegin to false
        if($.type(intBegin) !== "boolean")
        intBegin = false;

        characters += digits;
    }else{ hasNumbers = false; }

    if($.type(hasSymbols) === "boolean" && hasSymbols === true){
        // Set the default symbolBegin to false
        if($.type(symbolBegin) !== "boolean")
        symbolBegin = false;

        characters += symbols;
    }else{ hasSymbols = false; }

    // Calculate the invalid start
    if(!intBegin)
    invalidStart += digits;

    if(!symbolBegin)
    invalidStart += symbols;

    do{
        result += characters[generateRandomNumber(0, characters.length - 1)];

        if(result.length == 1 && invalidStart.indexOf(result) !== -1)
        result = generateRandomString(length, hasNumbers, intBegin, hasSymbols, symbolBegin);
    }while(result.length < length);

    return result;
}
