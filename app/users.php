<?php require_once('header.php'); ?>
<?php require_once('load/menu-top.php'); ?>
<?php require_once('load/menu-left.php'); ?>
<?php
if (isset($_SESSION['user']) && $_SESSION['user']['level'] < 50)
	Functions::redirect("main");

?>

	<div class="be-content">
		<div class="main-content container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<div id="users-wrapper">
						<?php require_once('load/users.php'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php require_once('load/menu-right.php'); ?>
<?php require_once('footer.php'); ?>
<?php require_once('foot.php'); ?>
