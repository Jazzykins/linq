<?php require_once('head.php'); ?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="Agents CRM">
	<meta name="author" content="W3Consultation">
	<link rel="shortcut icon" href="assets/img/logo-fav.png">
	<title><?php echo Config::WEBSITE_TITLE; ?></title>
	<base href="<?php echo Config::WEBSITE_URL; ?>/">
	<link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
	<link rel="stylesheet" type="text/css" href="assets/lib/daterangepicker/css/daterangepicker.css"/>
	<link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/lib/toastr/toastr.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/lib/select2/css/select2.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/lib/flipclock/flipclock.css"/>
	<link rel="stylesheet" type="text/css" href="assets/lib/summernote/summernote.css"/>
	<link rel="stylesheet" type="text/css" href="assets/lib/sweetalert2/dist/sweetalert2.css">
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/x-editable/bootstrap3-editable/css/bootstrap-editable.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/x-editable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datatables/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/daterangepicker/css/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.css"/>

	<link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
	<link rel="stylesheet" href="assets/css/custom.css" type="text/css"/>

	<script src="assets/lib/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
	<script src="assets/lib/jquery-cookie/jquery.cookie.js"></script>
</head>
<body>
<div class="be-wrapper be-fixed-sidebar">
