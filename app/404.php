<?php require_once('header.php'); ?>

<div class="wrapper-page">
	<div class=" card-box">
		<div class="row">
			<div class="col-md-12">
				<div class="col-middle">
					<div class="text-center text-center">
						<h1 class="error-number">404</h1>
						<h2>Sorry but we couldn't find this page</h2>
						<p>This page you are looking for does not exist <a href="#">Report this?</a>
						</p>
						<div class="mid_center">
							<a class="btn btn-default waves-effect waves-light" href="<?php echo Config::WEBSITE_URL ?>"> Return Home</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require_once('foot.php'); ?>