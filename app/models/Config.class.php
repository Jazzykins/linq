<?php
	/******************************************************
	*   Author          George Boursiquot <bgeorgealdly@gmail.com>
	*   Version         1.0.2
	*   Last modified   July 5th, 2017
	*   Web             http://w3consultation.com
	*   This is the main configuration file. It contains the
	*	configuration directives that gives the core and other
	*	files its' intructions.
	*******************************************************/
    abstract class Config{
		#--------------------------- Website Properties ---------------------------#
		# Website url with the trailing slash
		CONST WEBSITE_URL = "http://linq.com.local/app";

		# Website title / Name of the website
		CONST WEBSITE_TITLE = "LINQ";

        # The duration of the password reset key in hours (Default: 24)
        # Set to zero to have no expiration.
        CONST PASSWORD_RESET_KEY_DURATION = 24;

        # No permission error message
        CONST INVALID_PERMISSION_MESSAGE = "You do not have the permission to view the page you were trying to access.";

        # error message for db error
		CONST DEFAULT_DB_ERROR = "Oupps our system had a little hiccup, please try again!";

		# default email
		CONST DEFAULT_EMAIL = "info@linq.com";

		# default do-not-reply email
		CONST DEFAULT_NOREPLY_EMAIL = "do-not-reply@linq.com";

		#default website logo
        CONST DEFAULT_LOGO = "http://linq.com.local/app/assets/images/logo.png";

		#--------------------------- Core Properties ------------------------------#
		# Folder that contains the post actions without the trailing slash
		CONST POST_ACTION_PATH = "actions/post";

		# True to use version control (Default: true)
		# This will append the version to the post action path (Example: actions/post/1.0.0/)
		CONST INIT_POST_VERSION_CONTROL = false;

		# Default post action version for the core if post version control is enabled
		CONST DEFAULT_CORE_POST_VERSION = "1.0.0";

		# Folder that contains the get actions without the trailing slash
		CONST GET_ACTION_PATH = "actions/get";

		# True to use version control (Default: false)
		# This will append the version to the get action path (Example: actions/get/1.0.0/)
		CONST INIT_GET_VERSION_CONTROL = false;

		# Default get action version for the core if get version control is enabled
		CONST DEFAULT_CORE_GET_VERSION = "1.0.0";

		# True to use Tokenizer (Default: false)
		CONST INIT_TOKENIZER = true;

		# True to enable action case (Default: true)
		# Set to true if you want more than one action per file
		CONST INIT_ACTION_CASE = true;

        # True if you're debugging
        CONST DEBUG_CORE = true;

		#--------------------------------------------------------------------------#
    }
?>