<?php
define('LOG_FILE', "{$_SERVER['DOCUMENT_ROOT']}/user_action.log");

class DBManager {
	protected $db;

	//Singleton to make sure there are always only on DBManager instantiated
	public static function Instance(){
		static $inst = null;
		if($inst === null)
			$inst = new DBManager();

		return $inst;
	}

	private function __construct(){
		$host = 'localhost';
		$db = 'linq';
		$username = 'root';
		$password = '';

		try {
			$this->db = new PDO("mysql:host=$host;dbname=$db", $username, $password);
			$this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
			$this->db->exec("set names utf8");
		} catch (Exception $e) {
			die('[DBManager] Error: ' . $e->getMessage());
		}
	}

	public function getDb(){
		return $this->db;
	}

	/***** LOG ACTION SECTION *****/

	/*
	*	Function to log all the actions.
	*	@action: 	action performed.
	*	@user: 		user who performed the action
	*/
	public function write_to_log($action, $username, $user_id = null, $section = null, $entity_id = null){
		// Timestamp
		$text = '[' . date ('m/d/Y g:i A') . '] - ';
		$text .= $action . " - " . $username;

		// Write to log
		$fp = fopen (LOG_FILE , 'a');
		fwrite ($fp, $text . "\n");
		fclose ($fp ); // close file
		chmod (LOG_FILE , 0600);

        if(isset($user_id, $section, $entity_id)){
            // Save to DB:
            $query = $this->db->prepare("INSERT INTO logs (action_description, user_id, user_username, related_section, related_entity_id) VALUES (?, ?, ?, ?, ?)");
            $query->execute(array($action, $user_id, $username, $section, $entity_id));
        }
	}

    /**
     * [getAllLogs description]
     * @return [type] [description]
     */
    public function getAllLogs($section = null, $entity_id = null){
        if(isset($section, $entity_id)){
            $query = $this->db->prepare("SELECT * FROM logs WHERE related_section = ? AND related_entity_id = ? ORDER BY action_date DESC");
            if($query->execute(array($section, $entity_id))){
                return $query->fetchAll();
            } else {
                return false;
            }
        } else
            return $this->db->query("SELECT * FROM logs ORDER BY action_date DESC")->fetchAll();
    }
}
?>
