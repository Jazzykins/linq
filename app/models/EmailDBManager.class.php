<?php

class EmailDBManager extends DBManager {
	protected $db;
	/**
	 * Factory method to be able to call the class without instantiation
	 * @return EmailDBManager instance of DBManager for users
	 */
	public static function please(){
		return new EmailDBManager();
	}

	public function __construct(){
		$this->db = DBManager::Instance()->getDb();
	}

	/**
	 * Function to add an email in database.
	 * @param $arr: email data
	 * @return bool boolean: success state
	 */
	public function addEmail($arr){
		$query = $this->db->prepare("INSERT INTO emails VALUES(DEFAULT, ?, ?, DEFAULT)");
		if($query->execute(array(trim($arr['subject']), trim($arr['message'])))){
                $email_id = $this->db->lastInsertId();
				$this->write_to_log("New Email added", $_SESSION['user']['username'], $_SESSION['user']['user_id'], "emails", $email_id);
				return true;
		} else {
			return false;
		}
	}

	/**
	 * Function to edit a user in database.
	 * @param $arr: email data
	 * @return bool boolean: success state
	 */
	public function editEmail($arr){
		$query = $this->db->prepare("UPDATE emails SET email_subject = ?, email_message = ? WHERE email_id = ?");
		if($query->execute(array(trim($arr['subject']), trim($arr['message']), $arr['id']))) {
			    $this->write_to_log("Email edited", $_SESSION['user']['username'], $_SESSION['user']['user_id'], "emails", $arr['id']);
			    return true;
		} else {
			return false;
		}
	}

	/**
	* Function to delete an email.
	* @param $email_id: id of the email to delete.
	* @return bool boolean: success state
	*/
	public function deleteEmail($email_id){
		$query = $this->db->prepare("DELETE FROM emails WHERE email_id = ?");
		if($query->execute(array($email_id))){
			$this->write_to_log("Email deleted", $_SESSION['user']['username'], $_SESSION['user']['user_id'], "emails", $email_id);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Function to get all the emails from database
	 * @return array of emails
	 */
	public function getAllEmails(){
		$query = $this->db->query("SELECT * FROM emails ORDER BY email_id ASC");
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * Function to get a single email from database
	 * @param $id: email_id of email
	 * @return array|bool array on success or false on failure
	 */
	public function getSingleEmail($email_id){
		$query = $this->db->prepare("SELECT * FROM emails WHERE email_id = ?");
		$query->execute(array($email_id));
		return $query->fetch(PDO::FETCH_ASSOC);
	}
}
