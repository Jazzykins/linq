create table clients
(
	client_id int(10) unsigned auto_increment
		primary key,
	client_name varchar(50) not null,
	client_phone varchar(25) not null,
	client_email varchar(50) not null,
	client_address varchar(100) not null,
	client_status tinyint(1) default '1' not null
)
;

create table convos
(
	convo_id int auto_increment
		primary key,
	convo_user_id int not null,
	convo_receiver_id int not null,
	convo_date datetime default CURRENT_TIMESTAMP not null,
	convo_subject varchar(100) not null,
	convo_message varchar(1000) not null,
	convo_status tinyint(1) default '1' not null
)
;

create table emails
(
	email_id int auto_increment
		primary key,
	email_subject varchar(50) not null,
	email_message varchar(1000) not null,
	email_status tinyint(1) default '1' not null
)
;

create table logs
(
	action_description varchar(100) null,
	user_id int null,
	user_username varchar(50) null,
	related_section varchar(50) null,
	related_entity_id int null
)
;

create table registrations
(
	registration_id int auto_increment
		primary key,
	registration_name varchar(100) not null,
	registration_phone varchar(50) not null,
	registration_email varchar(100) not null,
	registration_comments varchar(1000) not null,
	registration_buying_frame varchar(50) not null,
	registration_bedrooms varchar(50) not null,
	registration_residence varchar(50) not null,
	registration_message varchar(1000) null,
	registration_status tinyint(1) default '1' not null
)
;

create table users
(
	user_id bigint auto_increment
		primary key,
	username varchar(15) default '' not null,
	password varchar(100) default '' not null,
	email varchar(100) not null,
	name varchar(50) default '' not null,
	level smallint default '0' not null,
	last_login datetime null,
	changed_password tinyint(1) default '0' not null,
	status smallint default '1' not null
)
;

create table users_keys
(
	user_key_id bigint auto_increment
		primary key,
	value varchar(255) default '' not null,
	type varchar(50) default '' not null,
	creation_time timestamp default CURRENT_TIMESTAMP not null,
	last_access_time timestamp null,
	user_fk bigint not null,
	constraint users_keys_ibfk_1
	foreign key (user_fk) references users (user_id)
		on update cascade
)
;

create index user_unique
	on users_keys (user_fk)
;

INSERT INTO linq.clients (client_name, client_phone, client_email, client_address, client_status) VALUES ('Georgealdy', '514-777-1212', 'george@email.ca', '1616 rene-levesque', 1);
INSERT INTO linq.clients (client_name, client_phone, client_email, client_address, client_status) VALUES ('Ayman Devil', '514-666-66663', 'devil@hell.ca', '123 hellish', 0);
INSERT INTO linq.clients (client_name, client_phone, client_email, client_address, client_status) VALUES ('Jacynthe Pratte', '5146799015', 'fece@ecr.bg', '676 Manning', 0);
INSERT INTO linq.clients (client_name, client_phone, client_email, client_address, client_status) VALUES ('Georgette Hammond', '5146799015', 'majdem@hotmail.com', '10384 Louis-Bonin', 0);
INSERT INTO linq.clients (client_name, client_phone, client_email, client_address, client_status) VALUES ('Jacynthe Pratte', '5146799015', 'asd@asd.ca', '676 Manning', 0);
INSERT INTO linq.emails (email_subject, email_message, email_status) VALUES ('woah', '<p>12345ukhgn</p><p>vdv</p>', 1);
INSERT INTO linq.logs (action_description, user_id, user_username, related_section, related_entity_id) VALUES ('New Email added', 44, 'devil', 'emails', 3);
INSERT INTO linq.logs (action_description, user_id, user_username, related_section, related_entity_id) VALUES ('New Email added', 44, 'devil', 'emails', 4);
INSERT INTO linq.logs (action_description, user_id, user_username, related_section, related_entity_id) VALUES ('Email edited', 44, 'devil', 'emails', 1);
INSERT INTO linq.logs (action_description, user_id, user_username, related_section, related_entity_id) VALUES ('Email edited', 44, 'devil', 'emails', 1);
INSERT INTO linq.logs (action_description, user_id, user_username, related_section, related_entity_id) VALUES ('Email deleted', 44, 'devil', 'emails', 1);
INSERT INTO linq.logs (action_description, user_id, user_username, related_section, related_entity_id) VALUES ('Email deleted', 44, 'devil', 'emails', 2);
INSERT INTO linq.logs (action_description, user_id, user_username, related_section, related_entity_id) VALUES ('Email deleted', 44, 'devil', 'emails', 4);
INSERT INTO linq.registrations (registration_name, registration_phone, registration_email, registration_comments, registration_buying_frame, registration_bedrooms, registration_residence, registration_message, registration_status) VALUES ('name', '1234567890', 'example@example.com', '', 'now', '3 bedrooms', 'Other', 'message', 0);
INSERT INTO linq.registrations (registration_name, registration_phone, registration_email, registration_comments, registration_buying_frame, registration_bedrooms, registration_residence, registration_message, registration_status) VALUES ('Jacynthe Pratte', '5146799015', 'mjadem@hotmail.com', '', '6 months', '3 bedrooms', 'Other', 'waehrtjyuiop[
''jhgffgr
f
g
r
g

rg
r
g
rg
r
g
rgrg', 0);
INSERT INTO linq.registrations (registration_name, registration_phone, registration_email, registration_comments, registration_buying_frame, registration_bedrooms, registration_residence, registration_message, registration_status) VALUES ('Jacynthe Pratte', '5146799015', 'mjadem@hotmail.com', '', '3 months', '2 bedrooms', 'West Island', '1
2
3
', 0);
INSERT INTO linq.registrations (registration_name, registration_phone, registration_email, registration_comments, registration_buying_frame, registration_bedrooms, registration_residence, registration_message, registration_status) VALUES ('Jacynthe Pratte', '5146799015', 'mjadem@hotmail.com', '', '3 months', '2 bedrooms', 'West Island', '1
2
3
', 0);
INSERT INTO linq.registrations (registration_name, registration_phone, registration_email, registration_comments, registration_buying_frame, registration_bedrooms, registration_residence, registration_message, registration_status) VALUES ('Jacynthe Pratte', '5146799015', 'mjadem@hotmail.com', 'im sexy!', 'now', '2 bedrooms', 'West Island', 'lol

zzz', 1);
INSERT INTO linq.registrations (registration_name, registration_phone, registration_email, registration_comments, registration_buying_frame, registration_bedrooms, registration_residence, registration_message, registration_status) VALUES ('Ayman Diab', '5146799015', 'aymandiab013@gmail.com', 'yay!', '12 months+', '3 bedrooms', 'Other', '', 1);
INSERT INTO linq.registrations (registration_name, registration_phone, registration_email, registration_comments, registration_buying_frame, registration_bedrooms, registration_residence, registration_message, registration_status) VALUES ('fucker', '848484684684', 'mjadem@hotmail.com', '', '6 months', '3 bedrooms', 'West Island', 'fuck off', 0);
INSERT INTO linq.registrations (registration_name, registration_phone, registration_email, registration_comments, registration_buying_frame, registration_bedrooms, registration_residence, registration_message, registration_status) VALUES ('dude', '741681618186', 'haha@ha.lol', 'jnkj
lkmjoij', 'now', '3 bedrooms', 'Vaudreuil', 'lol', 0);
INSERT INTO linq.users (username, password, email, name, level, last_login, changed_password, status) VALUES ('george', 'd3184f8f2d33e8a794dc244ecbb3163f', 'bgeorgealdly@gmail.com', 'George Boursiquot', 100, '2016-12-14 10:10:22', 1, 1);
INSERT INTO linq.users (username, password, email, name, level, last_login, changed_password, status) VALUES ('devil', 'd7d4925e4f3a70868405941208d6a3a0', 'mephistopheles13@hotmail.ca', 'Ayman Diab', 100, '2017-08-22 17:07:59', 1, 1);
INSERT INTO linq.users (username, password, email, name, level, last_login, changed_password, status) VALUES ('Jazz', '21232f297a57a5a743894a0e4a801fc3', 'jacynthe.pratt@gmail.com', 'Jacynthe', 100, '2017-08-15 12:14:58', 1, 1);
INSERT INTO linq.users_keys (value, type, creation_time, last_access_time, user_fk) VALUES ('H3pfCd3RLzVWvz6JFc', 'AUTO_LOGIN', '2017-06-30 14:16:27', '2017-07-02 16:01:53', 48);