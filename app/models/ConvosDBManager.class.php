<?php
/**
 * Created by PhpStorm.
 * User: Jazzykins
 * Date: 2017-08-14
 * Time: 9:53 PM
 */

class ConvosDBManager extends DBManager {
	protected $db;

	/**
	 * Factory method to be able to call the class without instantiation
	 * @return ConvosDBManager instance of DBManager for emails
	 */
	public static function please() {
		return new ConvosDBManager();
	}

	public function __construct() {
		$this->db = DBManager::Instance()->getDb();
	}

	public function addConvo($arr) {
		$query = $this->db->prepare("INSERT INTO convos VALUES(NULL,?,?,DEFAULT ,?,?,DEFAULT)");
		if ($query->execute(array($arr['user_id'], $arr['receiver_id'], $arr['subject'], '', $arr['message']))) {
			$this->write_to_log("New email added [Name: " . $arr['name'] . "]", $_SESSION['user']['username']);
			return $this->db->lastInsertId();
		} else
			return false;
	}

	/**
	 * Function to get a single property from database
	 * @param $id : id of property
	 * @return array|bool array on success or false on failure
	 */
	public function getSingleConvo($id) {
		$query = $this->db->prepare("SELECT * FROM convos WHERE convo_id = ?");
		$query->execute(array($id));
		return $query->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Function to get all the emails from database
	 * @param $user
	 * @param $receiver
	 * @return array of emails
	 */
	public function getConvo($user, $receiver) {
		$query = $this->db->prepare("SELECT * FROM convos JOIN registrations ON registration_id = convo_receiver_id WHERE convo_status = 1 AND convo_user_id = ? AND convo_receiver_id = ? ORDER BY convo_id ASC");
		$query->execute(array($user, $receiver));
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}

	public function editConvo($arr) {
		$query = $this->db->prepare("UPDATE convos SET convo_user_id = ?, convo_receiver_id = ?, convo_subject = ?, convo_message = ? WHERE convo_id = ?");
		return $query->execute(array($arr['user_id'], $arr['receiver_id'], $arr['subject'], '', $arr['message'], $arr['id']));
	}

	public function deleteConvo($id) {
		$query = $this->db->prepare("UPDATE convos SET convo_status = 0 WHERE convo_id = ?");
		return $query->execute(array($id));
	}


}