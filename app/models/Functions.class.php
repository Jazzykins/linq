<?php
	date_default_timezone_set("America/Montreal");
	
	abstract class Functions{
		CONST EMAIL_REGEX = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
		/*
		*	Function to remove any empty string field to null.
		*	@list: the list of data to check passed by reference.
		*/
		public static function convertToNull(Array &$list){
			foreach($list as $key => $value){
				if($value == "")
					$list[$key] = NULL;
			}
		}
		
		/*
		*	Function to check if a string is empty.
		*	@string: the string to check.
		*	@trim: true to trim white spaces, otherwise false. (Default: true)
		*/
		public static function isEmpty($string, $trim = true){
			return is_null($string) ? true : ($trim ? strlen(trim($string)) < 1 : strlen($string) < 1);
		}
			
		/*
		*	Function to check if a page is available to guests.
		*	@page: name of the page to check.
		*/
		public static function isPublicPage($page){
			switch($page){
				case "404":
				case "core":
				case "forgot-password":
				case "login":
				case "reset-password":
					return true;
				default:
					return false;
			}
		}
		
		/*
		*	Function to generate a success message that will be displayed on next page load.
		*	Note: recommended to use with redirect
		*/
		public static function generateErrorMessage($message){
			setcookie('error-message', $message, time() + 60 * 60, "/");
		}
		
		/*
		*	Function to generate a success message that will be displayed on next page load.
		*	Note: recommended to use with redirect
		*/
		public static function generateSuccessMessage($message){
			setcookie('success-message', $message, time() + 60 * 60, "/");
		}
		
		/*
		*	Function to redirect the user to a page and exit the current page.
		*	@destination: page to redirect after the website url.
		*	@delay: delay before redirect. (Default: 0)
		*/
		public static function redirect($destination = "", $delay = 0){
			echo '<meta http-equiv="refresh" content="' . $delay . ';url=' . Config::WEBSITE_URL . '/' . $destination . '">';
			exit;
		}
		
		/*
		*	Function to send en email.
		*	@from: Sender of the email.
		*	@to: Receiver of the email.
		*	@subject: Subject of the email.
		*	@message: Message of the email.
		*/
		public static function sendEmail($from, $to, $subject, $message, $name = Config::WEBSITE_TITLE, $inner = true){
			$logo = $inner==true?'<div style="margin:15px 0px;"><img src="' . str_replace('/app', '', Config::WEBSITE_URL) . '/app/assets/img/propertyhub.png" alt="Logo"></div>':'';
			$sub = $inner==false?'<a href="' . Config::WEBSITE_URL . '/unsubscribe/' . $to . '" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; color: #999; text-decoration: underline; margin: 0;">Unsubscribe</a>':'';
			$newsubject='=?UTF-8?B?'.base64_encode($subject).'?=';

			$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml" style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<head>
				<meta name="viewport" content="width=device-width" />
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<title>' . Config::WEBSITE_TITLE . '</title>

				<style type="text/css">
					img {
						max-width: 100%;
					}
					body {
						-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em;
					}
					body {
						background-color: #f6f6f6;
					}
					@media only screen and (max-width: 640px) {
						body {
							padding: 0 !important;
						}
						h1 {
							font-weight: 800 !important; margin: 20px 0 5px !important;
						}
						h2 {
							font-weight: 800 !important; margin: 20px 0 5px !important;
						}
						h3 {
							font-weight: 800 !important; margin: 20px 0 5px !important;
						}
						h4 {
							font-weight: 800 !important; margin: 20px 0 5px !important;
						}
						h1 {
							font-size: 22px !important;
						}
						h2 {
							font-size: 18px !important;
						}
						h3 {
							font-size: 16px !important;
						}
						.container {
							padding: 0 !important; width: 100% !important;
						}
						.content {
							padding: 0 !important;
						}
						.content-wrap {
							padding: 10px !important;
						}
						.invoice {
							width: 100% !important;
						}
					}
				</style>
			</head>

			<body style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">

				<table class="body-wrap" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6"><tr style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
					<td class="container" width="600" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
						' . $logo . '<div class="content" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
						' . $message . '<div class="footer" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">
						<table width="100%" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><tr style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="aligncenter content-block" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">'.$sub.'</td>
						</tr></table></div></div>
					</td>
					<td style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
				</tr></table></body>
				</html>';

			$message .= "\n." . SMTP::CRLF;
			//$mail = new PHPMailerOAuth;
			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->CharSet = 'UTF-8';
			$mail->SMTPDebug = SMTP::DEBUG_OFF; #0: no output, 1: server, 2:server and client
			$mail->Debugoutput = 'html';

			$mail->Host = 'a2plcpnl0402.prod.iad2.secureserver.net';
			$mail->Port = 25;
			$mail->SMTPAuth = false;
			$mail->SMTPSecure = false;

			$mail->setFrom($from, $name);
			$mail->addAddress($to, $name);
			$mail->Subject = $subject;
			$mail->msgHTML($message);
			return $mail->send();
		}

		/*
		*	Function to show a human readable date.
		*	@original_date: date to be converted in string or epoch time.
		*/
		public static function userFriendlyDate($date){
			if($date == NULL)
				return "Never";
				
			if(!is_numeric($date))
				$date = strtotime($date);
				
			$today = time();
			$YEAR = date('Y', $date);
			$MONTH = date('m', $date);
			$DAY = date('d', $date);
			$CURRENT_YEAR = date('Y', $today);	
			$CURRENT_MONTH = date('m', $today);	
			$CURRENT_DAY = date('d', $today);
			
			if($CURRENT_YEAR != $YEAR)
				return date("F Y", $date);
			else if($CURRENT_MONTH != $MONTH)
				return date("F d", $date);
			else
				return date("F jS", $date) . " at " . date("h:i a", $date);
				
			return date('F jS, Y', $date);
		}

		/*
        *   Function to do the actual validation.
        *   @data: array of the post.
        *   @key: name of the key in the array.
        *   @type: type of validation.
        *   @errorMsg: The error message container to update to become the error message to return.
        */
        private static function doValidateField($data, $key, $type, &$errorMsg){
            if(!isset($data[$key])){
                $errorMsg = "Please make sure the field " . $key . " is set.";
                return false;
            }
            
            # Reset the error message
            $errorMsg = "-1";
            
            if(is_numeric($type)){
				if(STATIC::isEmpty($data[$key]))
                    $errorMsg = "The ". $data[$key] ." cannot be empty.";
                else if(strlen(trim(strip_tags(html_entity_decode($data[$key])))) < $type)
					$errorMsg = "The ". $data[$key] ." must be at least " . $type . " characters.";
			}

			else if($type == "empty"){
				if(STATIC::isEmpty($data[$key]))
                    $errorMsg = "The ". str_replace(array("_fk", "_"), array("", " "), $data[$key]) ." cannot be empty.";
			}
			

			else if($type == "email"){
				if(STATIC::isEmpty($data[$key]))
                    $errorMsg = "The ". str_replace(array("_fk", "_"), array("", " "), $data[$key]) ." cannot be empty.";
                else if (!preg_match(STATIC::EMAIL_REGEX,$data[$key])){
                    $errorMsg = $data[$key] . "is not a valid email address.";
                }
			}

			else if(substr($type, 0, strlen('Gump:')) === 'Gump:'){
				
			}

            # Unknown validation type
            else
            	$errorMsg = "Unknown type: " . $type;
            
            return $errorMsg == "-1";
        }

        /*
        *   Function to check if a field is valid.
        *   @data: array of the post.
        *   @key: name of the key in the array.
        *   @type: type of validation.
        *   @errorMsg: The error message container to update to become the error message to return.
        */
        public static function isValidField(Array $data, $key, $type, &$errorMsg){
            return STATIC::doValidateField($data, $key, $type, $errorMsg);
        }
        
        /*
        *   Function to check if a list of fields is valid
        *   @data: the post array
        *   @key: list of the keys
        *   @type: list of the types of validation
        *   @errorMsg: the container of the error message that the function will return
        */
        public static function isValidFields(Array $data, Array $keys, Array $types, &$errorMsg){
            if(count($keys) != count($types)){
                $errorMsg = "Please make sure you have the same amount of key and type." ;
                return false;
            }
            
            # Loop through each keys
            for($i=0; $i<count($keys); $i++){
                $key = $keys[$i];
                $type = $types[$i];
                
                if(!STATIC::doValidateField($data, $key, $type, $errorMsg))
                    return false;
            }
            
            return true;
        }

        /**
         * validation function using the GUMP class
         * @param  array $data      data to validate
         * @param  array $rules     rules for each data
         * @param  array $filters   filter to sanitize data
         * @param  object &$errorMsg error handler
         * @return boolean or error array
         */
        public static function validateArray($data, $rules, $filters, &$errorMsg){
        	$Validation = new Gump();
            $data = $Validation->sanitize($data);

            $Validation->validation_rules($rules);
			$Validation->filter_rules($filters);

            $validated_data = $Validation->run($data);
			if($validated_data === false) {
				$errorMsg = $Validation->get_readable_errors(true);
				return false;
			}else{
				return true;
			}
        }

        /**
         * Validation for logo images
         * @param  array &$post     post by ref
         * @param  [type] $data      [description]
         * @param  [type] &$errorMsg [description]
         * @return [type]            [description]
         */
        public static function validateLogo(&$name, $data, &$errorMsg){
        	if(!$data['error']){
				$types = array("image/jpeg", "image/jpg", "image/png", "image/gif");
				if($data['size'] > 1048000){
					$errorMsg = "The image size is too big!";
					return false;
				}
				if(!in_array(strtolower($data['type']), $types)){
					$errorMsg = "Invalid image type!";
					return false;
				}
			}
			if(empty($data['error'])){
				$filename = preg_replace("/[^a-zA-Z0-9.]/", "", str_replace(",", "", str_replace(' ', '-', $_POST['name'] . "_" . time()))) . ".jpg";
				move_uploaded_file($data['tmp_name'], "assets/logos/" . $filename);
				$name = $filename;
				return true;
			}else{
				if($data['error'] == 4){
					//$filename = "default-logo.png";
					return true;
				}else{
					$errorMsg = $data['error'];
					return false;
				}
				return false;
			}
        }

        /*
		*	Function to move a file.
		*	@target: the file to move.
		*	@destination: the location we want to move the file to.
		*/
		public static function moveFile($target, $destination){
			if($target != NULL && $destination != NULL){
				if(file_exists($target)){
					copy($target, $destination);
					unlink($target);
					return true;
				}
			}
			return false;
		}

		public static function struuid($entropy){
			$s=uniqid("",$entropy);
			$num= hexdec(str_replace(".","",(string)$s));
			$index = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$base= strlen($index);
			$out = '';
			for($t = floor(log10($num) / log10($base)); $t >= 0; $t--) {
				$a = floor($num / pow($base,$t));
				$out = $out.substr($index,$a,1);
				$num = $num-($a*pow($base,$t));
			}
			return $out;
		}

	}
?>