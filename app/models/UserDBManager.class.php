<?php

class UserDBManager extends DBManager {
	protected $db;
	/**
	 * Factory method to be able to call the class without instantiation
	 * @return UserDBManager instance of DBManager for users
	 */
	public static function please(){
		return new UserDBManager();
	}

	public function __construct(){
		$this->db = DBManager::Instance()->getDb();
	}

	/**
	 * Function to add a user in database.
	 * @param $arr: user data
	 * @return bool boolean: success state
	 */
	public function addUser($arr){
		$query = $this->db->prepare("INSERT INTO users VALUES(DEFAULT, ?, ?, ?, ?, ?, NULL, DEFAULT, DEFAULT)");

		if($query->execute(array(trim($arr['username']), md5($arr['password']), trim($arr['email']), trim($arr['name']), $arr['level']))){
            $user_id = $this->db->lastInsertId();
			$this->write_to_log(
                "New User added [ID: " . $user_id . "]",
                $_SESSION['user']['username'],
                $_SESSION['user']['user_id'],
                "tasks",
                $user_id
            );
			return true;
		}else
			return false;
	}

	/**
	 * Function to delete a user -> set status to 0.
	 * @param $userId: id of the user to update
	 * @return bool boolean: success state
	 */
	public function deleteUser($user_id){
		$query = $this->db->prepare("UPDATE users SET status = " . User::DELETED . " WHERE user_id = ?");

		if($query->execute(array($user_id))){
			$this->write_to_log(
                "User deleted [ID: " . $user_id . "]",
                $_SESSION['user']['username'],
                $_SESSION['user']['user_id'],
                "tasks",
                $user_id
            );
			return true;
		}else
			return false;
	}

	/**
	 * Function to delete a user key.
	 * @param $keyId - id of the key
	 */
	public function deleteUserKey($keyId){
		$query = $this->db->prepare("DELETE FROM users_keys WHERE user_key_id = ?");
		$query->execute(array($keyId));
	}

	/**
	 * Function to edit a user in database.
	 * @param $arr: form array
	 * @return bool boolean: success state
	 */
	public function editUser($arr){
		$query = $this->db->prepare("UPDATE users SET username = ?, email = ?, name = ?, level = ? WHERE user_id = ?");

		if($query->execute(array(trim($arr['username']), trim($arr['email']), trim($arr['name']), $arr['level'], $arr['user_id']))){
			$this->write_to_log(
                "User edited [ID: " . $arr['user_id'] . "]",
                $_SESSION['user']['username'],
                $_SESSION['user']['user_id'],
                "tasks",
                $arr['user_id']
            );
			return true;
		}else
			return false;
	}

	/**
	 * Function to generate a temporary key for the user.
	 * @param $userId: id of the user.
	 * @param $value: value of the key.
	 * @param $type: type of the key.
	 */
	public function generateUserKey($userId, $value, $type){
		$query = $this->db->prepare("INSERT INTO users_keys(value, type, user_fk) VALUES(?, ?, ?);");
		$query->execute(array($value, $type, $userId));
	}

	/**
	 * Function to get a user by id.
	 * @param $userId: id of the user we want to get.
	 * @return array|bool array on success or false on failure
	 */
	public function getUser($userId){
		$query = $this->db->prepare("SELECT * FROM users WHERE user_id = ?");
		$query->execute(array($userId));
		return $query->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Function to get a user by email.
	 * @param $email: email of the user.
	 * @return array|bool array on success or false on failure
	 */
	public function getUserByEmail($email){
		$query = $this->db->prepare("SELECT * FROM users WHERE email = ? AND status NOT IN (" . User::DELETED . ", " . User::BANNED .")");
		$query->execute(array($email));
		return $query->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Function to get a key by it's value and type.
	 * @param $value: value of the key.
	 * @param $type: type of the key.
	 * @return array|bool array on success or false on failure
	 */
	public function getUserKey($value, $type){
		$query = $this->db->prepare("SELECT * FROM users_keys WHERE value = ? AND type = ?");
		$query->execute(array($value, $type));
		return $query->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Function to get a key by an user id and type.
	 * @param $userId: id of the user.
	 * @param $type: type of the key.
	 * @return array|bool array on success or false on failure
	 */
	public function getUserKeyByUserId($userId, $type){
		$query = $this->db->prepare("SELECT * FROM users_keys WHERE user_fk = ? AND type = ?");
		$query->execute(array($userId, $type));
		return $query->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Function to get all the users from database
	 * @return array of users
	 */
	public function getUsers(){
		$query = $this->db->query("SELECT * FROM users WHERE status = " . User::ACTIVE . " ORDER BY name ASC");
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * Function to get a single user from database
	 * @param $id: id of user
	 * @return array|bool array on success or false on failure
	 */
	public function getSingleUser($id){
		$query = $this->db->prepare("SELECT * FROM users WHERE user_id = ?");
		$query->execute(array($id));
		return $query->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Function to reload the profile of a user.
	 * @param $userId: id of the user to reload.
	 * @return array|bool array on success or false on failure
	 */
	public function reloadProfile($userId){
		$query = $this->db->prepare("SELECT *
			FROM users AS u
			WHERE user_id = ?");
		$query->execute(array($userId));

		return $query->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Function to reset a user's password.
	 * @param $userId: id of the user to reset password.
	 */
	public function resetPassword($userId){
		$query = $this->db->prepare("UPDATE users SET password = md5(''), changed_password = 0 WHERE user_id = ?");
		$query->execute(array($userId));
	}

	/**
	 * Function to check if the login credentials are correct.
	 * @param $login: username or email of the user.
	 * @param $password: password of the user without encryption.
	 * @return array|bool array on success or false on failure
	 */
	public function signIn($login, $password){
		$query = $this->db->prepare("SELECT * FROM users WHERE (username = ? OR email = ?) AND password = ?");
		$query->execute(array($login, $login, md5($password)));

		return $query->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Function to update the last login time of an user.
	 * @param $userId: id of the user to update
	 */
	public function updateLastLogin($userId){
		$query = $this->db->prepare("UPDATE users SET last_login = NOW() WHERE user_id = ?");
		$query->execute(array($userId));
	}

	/**
	 * Function to update a user's password.
	 * @param $userId: id of the user
	 * @param $password: password without encryption
	 */
	public function updatePassword($userId, $password){
		$query = $this->db->prepare("UPDATE users SET password = ?, changed_password = 1 WHERE user_id = ?");
		$query->execute(array(md5($password), $userId));
	}

	/**
	 * Function to update the last usage of a key.
	 * @param $keyId: id of the key to update.
	 */
	public function updateUserKeyUsage($keyId){
		$query = $this->db->prepare("UPDATE users_keys SET last_access_time = NOW() WHERE user_key_id = ?");
		$query->execute(array($keyId));
	}
}
