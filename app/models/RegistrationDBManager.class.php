<?php
/**
 * Created by PhpStorm.
 * User: Jazzykins
 * Date: 2017-08-14
 * Time: 9:53 PM
 */

class RegistrationDBManager extends DBManager {
	protected $db;

	/**
	 * Factory method to be able to call the class without instantiation
	 * @return RegistrationDBManager instance of DBManager for emails
	 */
	public static function please() {
		return new RegistrationDBManager();
	}

	public function __construct() {
		$this->db = DBManager::Instance()->getDb();
	}

	public function addRegistration($arr) {
		$query = $this->db->prepare("INSERT INTO registrations VALUES(NULL,?,?,?,?,?,?,?,?,default)");
		if ($query->execute(array($arr['name'], $arr['phone'], $arr['email'], '', $arr['buying_frame'], $arr['bedrooms'], $arr['residence'], $arr['message']))) {
			$this->write_to_log("New email added [Name: " . $arr['name'] . "]", $_SESSION['user']['username']);
			return $this->db->lastInsertId();
		} else
			return false;
	}

	/**
	 * Function to get a single property from database
	 * @param $id : id of property
	 * @return array|bool array on success or false on failure
	 */
	public function getSingleRegistration($id) {
		$query = $this->db->prepare("SELECT * FROM registrations WHERE registration_id = ?");
		$query->execute(array($id));
		return $query->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Function to get all the emails from database
	 * @return array of emails
	 */
	public function getRegistration() {
		$query = $this->db->query("SELECT * FROM registrations WHERE registration_status = 1 ORDER BY registration_id ASC");
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}

	public function editRegistration($arr) {
		$query = $this->db->prepare("UPDATE registrations SET registration_name  = ?, registration_phone = ?, registration_email = ?, registration_buying_frame = ?, registration_bedrooms = ?, registration_residence = ?, registration_message = ? WHERE registration_id = ?");
		return $query->execute(array($arr['name'], $arr['phone'], $arr['email'], $arr['buying_frame'], $arr['bedrooms'], $arr['residence'], $arr['message'], $arr['id']));
	}

	public function deleteRegistration($id) {
		$query = $this->db->prepare("UPDATE registrations SET registration_status = 0 WHERE registration_id = ?");
		return $query->execute(array($id));
	}

	public function addComments($id, $comment) {
		$query = $this->db->prepare("UPDATE registrations SET registration_comments = ? WHERE registration_id = ?");
		return $query->execute(array($comment, $id));
	}

}