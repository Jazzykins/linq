<?php
if (file_exists("../head.php"))
	include("../head.php");

$dynamicFormId = Tokenizer::get('dynamic-form-id', Tokenizer::GET_TOKEN);

if (isset($_GET['id'])) {
	$id = IDObfuscator::decode($_GET['id']);
} else {
	Functions::redirect("registrations");
}

$convo = ConvosDBManager::please()->getConvo($_SESSION['user']['user_id'], $id);
$registration = RegistrationDBManager::please()->getSingleRegistration($id);
$emails = EmailDBManager::please()->getAllEmails();

//tokens
$registrationActionToken = Tokenizer::add('post-action-registration', 20, 'registration');
$addRegistrationCaseToken = Tokenizer::add('post-case-add', 30, 'add');
$editRegistrationCaseToken = Tokenizer::add('post-case-edit', 30, 'edit');
$deleteRegistrationCaseToken = Tokenizer::add('post-case-delete', 30, 'delete');
$singleEditRegistrationCaseToken = Tokenizer::add('post-case-single-edit', 30, 'single-edit');
$singleViewRegistrationCaseToken = Tokenizer::add('post-case-single-view', 30, 'single-view');
$commentsRegistrationCaseToken = Tokenizer::add('post-case-comments', 30, 'comments');

$emailActionToken = Tokenizer::add('post-action-email', 20, 'email');
$addEmailCaseToken = Tokenizer::add('post-case-add', 30, 'add');
$editEmailCaseToken = Tokenizer::add('post-case-edit', 30, 'edit');
$deleteEmailCaseToken = Tokenizer::add('post-case-delete', 30, 'delete');
$singleEditEmailCaseToken = Tokenizer::add('post-case-single-edit', 30, 'single-edit');
$singleViewEmailCaseToken = Tokenizer::add('post-case-single-view', 30, 'single-view');
$singleSendEmailCaseToken = Tokenizer::add('post-case-single-send', 30, 'single-send');
$sendEmailCaseToken = Tokenizer::add('post-case-send', 30, 'send');

var_dump($convo);

?>

<div class="row">
	<div class="col-sm-6">
		<div class="panel panel-default panel-border-color panel-border-color-primary">
			<div class="panel-heading">
				<h2 class="pull-left">History</h2>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body">
				<div class="p-20">
					<div id="convos" class="tab-pane fade in active cont">
						<?php
						$total = 0;
						foreach ($convo as $value) {
							if (true) {
								$total++;
								?>
								<div class="well well-sm">
									<h5 class="pull-right"><i
												class="icon mdi mdi-time"></i> <?php echo Functions::userFriendlyDate($value['convo_date']) ?>
									</h5>
									<h3 class="pull-left"><?php echo $value['convo_subject'] ?></h3>
									<div class="clearfix"></div>
									<p><?php echo $value['convo_message'] ?></p>
								</div>
								<?php
							}
						}
						?>
						<table class="table data-table table-striped table-hover table-fw-widget">
							<thead>
							<tr>
								<th>Subject</th>
								<th>Message</th>
								<th>Date</th>
							</tr>
							</thead>
							<tbody>
							<?php
							$total = 0;
							foreach ($convo as $value) {
								if (true) {
									$total++;
									?>
									<tr>
										<td><?php echo $value['convo_subject'] ?></td>
										<td><?php echo $value['convo_message'] ?></td>
										<td><?php echo Functions::userFriendlyDate($value['convo_date']) ?></td>
									</tr>
									<?php
								}
							}
							?>
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="panel panel-default panel-border-color panel-border-color-primary">
			<div class="panel-heading">
				<h2 class="pull-left">Actions</h2>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body">
				<div class="tab-container">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#notes" data-toggle="tab">
								<span class="icon mdi mdi-comment-edit"></span>Notes</a>
						</li>
						<li><a href="#email" data-toggle="tab">
								<span class="icon mdi mdi-mail-send"></span>Send Email</a>
						</li>
					</ul>
					<div class="tab-content">
						<div id="notes" class="tab-pane fade in active cont">
							<form class="form-horizontal" role="form" data-parsley-validate="" novalidate>
								<input type="hidden" name="action" value="<?php echo $registrationActionToken ?>">
								<input type="hidden" name="case" value="<?php echo $singleEditRegistrationCaseToken ?>">
								<form>
							<textarea title="comments" class="form-control" name="comments" rows="10"
							          data-id="<?php echo $registration['registration_id'] ?>"
							><?php echo $registration['registration_comments'] ?></textarea>
								</form>
							</form>
						</div>
						<div id="email" class="tab-pane fade cont">
							<form class="form-horizontal" role="form" data-parsley-validate="" novalidate>
								<input type="hidden" name="action" value="<?php echo $emailActionToken ?>">
								<input type="hidden" name="case" value="<?php echo $sendEmailCaseToken ?>">
								<input type="hidden" name="ids" id="ids" value="">
								<div class="main-content container-fluid">
									<div class="col-sm-6">
										<label for="email-select">Choose an email</label>
										<select class="select2" name="email_id" id="email-select">
											<option value="">---</option>
											<?php foreach ($emails as $email): ?>
												<option value="<?php echo $email['email_id'] ?>"><?php echo $email['email_subject'] ?></option>
											<?php endforeach; ?>
											<option value="blank">New email</option>
										</select>
									</div>
								</div>
								<div class="main-content container-fluid email-container" style="display: none;">
									<hr>
									<div class="email-head">
										<div class="email-head-subject">
											<div class="title">
												<span id="email-title"></span>
											</div>
										</div>
										<div class="email-head-sender">
											<div class="date"><?php echo Functions::userFriendlyDate(date(time())); ?></div>
											<div class="sender">
												Receivers:
												<div class="actions">
													<a href="#" class="ccToggle"><i class="mdi mdi-caret-down"></i></a>
												</div>
												<div class="cc_container"></div>
												<div class="row carbon-copies">
													<div class="col-sm-8">
														<select name="carbon_copies[]" class="form-control input-sm tags"
														        id="cc-tags" multiple>
															<option selected
															        value='<?php echo $registration['registration_email'] ?>'><?php echo $registration['registration_email'] ?></option>
														</select>
													</div>
													<div class="col-sm-2">
														<button type="button" class="btn btn-primary ccToggle"><i
																	class="glyphicon glyphicon-ok"></i></button>
														<button type="button" class="btn btn-default ccCancel"><i
																	class="glyphicon glyphicon-remove"></i></button>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="email-body"></div>
									<div class="col-sm-12">
										<hr>
										<button type="submit" class="btn btn-primary btn-xl">Send <i
													class="mdi mdi-mail-send"></i></button>
									</div>
								</div>
								<div class="main-content container-fluid blank-email-container" style="display: none;">
									<div class="form-group">
										<label for="">Subject</label>
										<input type="text" class="form-control" name="subject" placeholder=""
										       id="subject-blank">
									</div>
									<div class="form-group">
										<label for="cc-email-blank">CC</label>
										<select name="carbon_copies[]" class="tags" multiple>
											<option selected
											        value='<?php echo $registration['registration_email'] ?>'><?php echo $registration['registration_email'] ?></option>
										</select>
									</div>
									<div class="form-group">
										<label for="email-wyziwyg">Message</label>
										<textarea name="message" class="email-wyziwyg"></textarea>
									</div>
									<div class="form-group pull-right" style="padding-top: 22px">
										<button type="submit" name="send_email" class="btn btn-xl btn-primary">Send email <i
													class="mdi mdi-mail-send"></i></button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="view-modal" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
	<div class="modal-dialog custom-width">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" class="close md-close"><span
							class="mdi mdi-close"></span></button>
				<h3 class="modal-title"><span class="mdi mdi-info-outline"></span> Registration Information</h3>
			</div>
			<div class="modal-body">
				<table class="table table-striped table-hover ">
					<tr>
						<th>Name</th>
						<td data-name=""></td>
					</tr>
					<tr>
						<th>Phone</th>
						<td data-phone=""></td>
					</tr>
					<tr>
						<th>Email</th>
						<td data-email=""></td>
					</tr>
					<tr>
						<th>Buying frame</th>
						<td data-buying_frame=""></td>
					</tr>
					<tr>
						<th>Number of Bedrooms</th>
						<td data-bedrooms=""></td>
					</tr>
					<tr>
						<th>Current residence</th>
						<td data-residence=""></td>
					</tr>
					<tr>
						<th>Message</th>
						<td data-message=""></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="email-modal" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
	<div class="modal-dialog custom-width">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" class="close md-close"><span
							class="mdi mdi-close"></span></button>
				<h3 class="modal-title">
					<span class="mdi mdi-edit"></span>
					Add New Registration
				</h3>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" data-parsley-validate="" novalidate>
					<input type="hidden" name="action"
					       value="<?php echo $addRegistrationActionToken; ?>">
					<input type="hidden" name="case"
					       value="<?php echo $addRegistrationCaseToken; ?>">
					<input type="hidden" name="id" id="id" value="">

					<div class="form-group">
						<label for="name" class="col-sm-4 control-label">Name</label>
						<div class="col-sm-8">
							<input class="form-control" required="" id="name" name="name"
							       placeholder="Name">
						</div>
					</div>

					<div class="form-group">
						<label for="phone" class="col-sm-4 control-label">Phone</label>
						<div class="col-sm-8">
							<input class="form-control" required="" id="phone" name="phone"
							       placeholder="Phone">
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="col-sm-4 control-label">Email</label>
						<div class="col-sm-8">
							<input class="form-control" required="" id="email" name="email"
							       placeholder="Email">
						</div>
					</div>

					<div class="form-group">
						<label for="buying_frame" class="col-sm-4 control-label">Buying frame</label>
						<div class="col-sm-8">
							<select class="form-control" name="buying_frame" id="buying_frame">
								<option value="now">Now</option>
								<option value="3 months">3 months</option>
								<option value="6 months">6 months</option>
								<option value="12 months+">12 months or more</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="name" class="col-sm-4 control-label">Number of bedrooms</label>
						<div class="col-sm-8">
							<div class="be-radio">
								<input id="2bedrooms" class=" radio-style" name="bedrooms" value="2 bedrooms"
								       type="radio" checked>
								<label for="2bedrooms" class="radio-style-1-label radio-small">2 Bedrooms</label>
							</div>
							<div class="be-radio">
								<input id="3bedrooms" class="  radio-style" name="bedrooms" value="3 bedrooms"
								       type="radio">
								<label for="3bedrooms" class="radio-style-2-label radio-small">3 Bedrooms</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label" for="residence">Current Residence</label>
						<div class="col-sm-8">
							<div class="be-radio">
								<input id="vaudreuil_form" class=" radio-style" name="residence" value="Vaudreuil"
								       type="radio" checked>
								<label for="vaudreuil_form"
								       class="radio-style-1-label radio-small">Vaudreuil</label>
							</div>
							<div class="be-radio">
								<input id="westIsland_form" class=" radio-style" name="residence"
								       value="West Island" type="radio">
								<label for="westIsland_form" class="radio-style-2-label radio-small">West
									Island</label>
							</div>
							<div class="be-radio">
								<input id="otherResidence_form" class="radio-style" value="Other"
								       name="residence" type="radio">
								<label for="otherResidence_form"
								       class="radio-style-2-label radio-small">Other</label>
							</div>
						</div>

					</div>

					<div class="form-group">
						<label class="control-label col-sm-4" for="message">
							Message
						</label>
						<div class="col-sm-8">
                                        <textarea class="form-control" id="message" name="message" cols="10"
                                                  rows="10"></textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-8">
							<button name="submit" class="btn btn-primary btn-xl w-xs">Add</button>
							<button type="reset" class="btn btn-danger btn-xl m-l-5 w-xs"
							        data-dismiss="modal">Cancel
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	$(function () {
		var modal = $("#registration-modal");
		var emailModal = $("#email-modal");
		var emailSendModal = $("#email-send-modal");
		var body = $("body");
		var registrationTable = $(".data-table");
		var emailTable = $(".email-data-table");
		var recordCheckBox = $('.recordCheckBox');

		App.formElements();

		body.on('blur', 'textarea[name="comments"]', function () {
			$('#<?php echo $dynamicFormId; ?>').append('<input type="hidden" name="action" value="<?php echo $registrationActionToken; ?>">'
				+ '<input type="hidden" name="case" value="<?php echo $commentsRegistrationCaseToken; ?>">'
				+ '<input type="hidden" name="id" value="' + $(this).data('id') + '">'
				+ '<textarea type="hidden" name="comments">' + $(this).val() + '</textarea>');
			$('#<?php echo $dynamicFormId; ?>').submit();
			$('#<?php echo $dynamicFormId; ?>').empty();
		});

		$("#cc-tags").select2({
			tags: true,
			multiple: true,
			width: "100%",
			placeholder: 'CC'
		});

		$('#email-select').change(function () {
			if ($(this).val() === "") {
				$('.email-container').hide(600);
			} else if ($(this).val() !== "") {
				$('#<?php echo $dynamicFormId ?>').append(
					'<input type="hidden" name="action" value="<?php echo $emailActionToken; ?>">' +
					'<input type="hidden" name="case" value="<?php echo $singleSendEmailCaseToken; ?>">' +
					'<input type="hidden" name="id" value="' + $(this).val() + '">'
				);
				$('#<?php echo $dynamicFormId ?>').submit();
				$('#<?php echo $dynamicFormId ?>').empty();
			}
		});

		$('.ccToggle').click(function (e) {
			e.preventDefault();
			var flag = 0;
			$('#cc-tags').val().forEach(function (i, j) {
				flag++;
				if (j == 0) {
					$('.cc_container').empty().append(
						'<span class="text-muted">Cc:</span>'
					);
				}
				$('.cc_container').append(
					'<span class="text-muted">' + i + '</span>'
				);
			});
			if (flag) {
				$('.cc_container').show(300);
			}
			$('.carbon-copies').toggle(600);
		});


		$('.ccCancel').click(function () {
			$('#cc-tags').val('').trigger('change');
			$('.cc_container').hide(300);
			$('.carbon-copies').hide(600);
			$('.cc_container').empty();
		});

		$('.email-wyziwyg').summernote({
			height: 300,
			toolbar: [
				['style', ['bold', 'italic', 'underline']],
				['font', ['strikethrough', 'superscript', 'subscript']],
				['fontsize', ['fontsize']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['height', ['height']]
			]
		});
	})
	;

</script>
