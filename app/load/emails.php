<?php
//TODO: add ids to the hidden input on cc focus out... meh...
//TODO: check if it actually mails?
if (file_exists("../head.php"))
	include("../head.php");

$dynamicFormId = Tokenizer::get('dynamic-form-id', Tokenizer::GET_TOKEN);
$emails = EmailDBManager::please()->getAllEmails();

//tokens
$emailActionToken = Tokenizer::add('post-action-email', 20, 'email');
$addEmailCaseToken = Tokenizer::add('post-case-add', 30, 'add');
$editEmailCaseToken = Tokenizer::add('post-case-edit', 30, 'edit');
$deleteEmailCaseToken = Tokenizer::add('post-case-delete', 30, 'delete');
$singleEditEmailCaseToken = Tokenizer::add('post-case-single-edit', 30, 'single-edit');
$singleViewEmailCaseToken = Tokenizer::add('post-case-single-view', 30, 'single-view');
$singleSendEmailCaseToken = Tokenizer::add('post-case-single-send', 30, 'single-send');
$sendEmailCaseToken = Tokenizer::add('post-case-send', 30, 'send');

?>
<div class="panel panel-default panel-border-color panel-border-color-primary">
	<div class="panel-heading text-right">
		<a class="btn btn-primary btn-xl" href="#email-modal" data-case="new" data-toggle="modal">
			New Email <i class="mdi mdi-plus"></i>
		</a>
		<a class="btn btn-primary btn-xl" id="send-email-btn" href="#email-send-modal" data-case="new"
		   data-toggle="modal">
			Send Email <i class="mdi mdi-mail-send"></i>
		</a>
		<div class="clearfix"></div>
	</div>
	<div class="panel-body">
		<div class="p-20">
			<div id="emails">
				<h2>Emails</h2>
				<table class="table email-data-table table-striped table-hover table-fw-widget">
					<thead>
					<tr>
						<th>Subject</th>
						<th>Message</th>
						<th><i class="mdi mdi-wrench"></i></th>
					</tr>
					</thead>
					<tbody>
					<?php

					$total = 0;
					foreach ($emails as $value) {

						if (true) {
							$total++;
							?>
							<tr>
								<td><?php echo $value['email_subject'] ?></td>
								<td><?php echo $value['email_message'] ?></td>
								<td class="actions">
									<a href="#email-view-modal" data-toggle="modal"
									   title="View Info"
									   data-id="<?php echo $value['email_id'] ?>"
									   class="icon tableToolInfo">
										<i class="mdi mdi-info"></i>
									</a>
									<a href="#email-modal" data-toggle="modal"
									   title="Edit"
									   data-id="<?php echo $value['email_id'] ?>"
									   class="icon tableToolEdit" data-case="edit">
										<i class="mdi mdi-edit"></i>
									</a>
									<a href="#" data-id="<?php echo $value['email_id'] ?>"
									   title="Delete"
									   class="icon emailTableToolDelete">
										<i class="mdi mdi-delete"></i>
									</a>
								</td>
							</tr>
							<?php
						}
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
		<p class="text-muted font-13">
			Total of
			<span class="users-count">
				<?php echo $total; ?>Registration<?php echo $total != 1 ? 's' : '' ?>
			</span>
		</p>
	</div>
</div>

<div id="email-view-modal" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
	<div class="modal-dialog custom-width">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" class="close md-close"><span
							class="mdi mdi-close"></span></button>
				<h3 class="modal-title"><span class="mdi mdi-info-outline"></span> Registration Information</h3>
			</div>
			<div class="modal-body">
				<div class="col-sm-12">
					<h2 data-subject=""></h2>
				</div>
				<div class="col-sm-12">
					<div data-message=""></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="email-send-modal" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
	<div class="modal-dialog custom-width" style="max-width: 100%;width: 1224px;">
		<div class="modal-content" style="max-width: 90%;width: 100%;">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" class="close md-close"><span
							class="mdi mdi-close"></span></button>
				<h3 class="modal-title"><span class="mdi mdi-info-outline"></span> Registration Information</h3>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" data-parsley-validate="" novalidate>
					<input type="hidden" name="action" value="<?php echo $emailActionToken ?>">
					<input type="hidden" name="case" value="<?php echo $sendEmailCaseToken ?>">
					<input type="hidden" name="ids" id="ids" value="">
					<div class="main-content container-fluid">
						<div class="col-sm-6">
							<label for="email-select">Choose an email</label>
							<select class="select2" name="email_id" id="email-select">
								<option value="">---</option>
								<?php foreach ($emails as $email): ?>
									<option value="<?php echo $email['email_id'] ?>"><?php echo $email['email_subject'] ?></option>
								<?php endforeach; ?>
								<option value="blank">New email</option>
							</select>
						</div>
					</div>
					<div class="main-content container-fluid email-container" style="display: none;">
						<hr>
						<div class="email-head">
							<div class="email-head-subject">
								<div class="title">
									<span id="email-title"></span>
								</div>
							</div>
							<div class="email-head-sender">
								<div class="date"><?php echo Functions::userFriendlyDate(date(time())); ?></div>
								<div class="sender">
									Receivers:
									<div class="actions">
										<a href="#" class="ccToggle"><i class="mdi mdi-caret-down"></i></a>
									</div>
									<div class="cc_container"></div>
									<div class="row carbon-copies">
										<div class="col-sm-8">
											<select name="carbon_copies[]" class="form-control input-sm tags"
											        id="cc-tags" multiple>
												<?php
												foreach ($emailList as $email)
													echo "<option value='$email'>$email</option>";
												?>
											</select>
										</div>
										<div class="col-sm-2">
											<button type="button" class="btn btn-primary ccToggle"><i
														class="glyphicon glyphicon-ok"></i></button>
											<button type="button" class="btn btn-default ccCancel"><i
														class="glyphicon glyphicon-remove"></i></button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="email-body"></div>
						<div class="col-sm-12">
							<hr>
							<button type="submit" class="btn btn-primary btn-xl">Send <i class="mdi mdi-mail-send"></i>
							</button>
						</div>
					</div>
					<div class="main-content container-fluid blank-email-container" style="display: none;">
						<div class="form-group">
							<label for="">Subject</label>
							<input type="text" class="form-control" name="subject" placeholder="" id="subject-blank">
						</div>
						<div class="form-group">
							<label for="cc-email-blank">CC</label>
							<select name="carbon_copies[]" class="tags" multiple>
								<?php
								foreach ($emailList as $email)
									echo "<option value='$email'>$email</option>";
								?>
							</select>
						</div>
						<div class="form-group">
							<label for="email-wyziwyg">Message</label>
							<textarea name="message" class="email-wyziwyg"></textarea>
						</div>
						<div class="form-group pull-right" style="padding-top: 22px">
							<button type="submit" name="send_email" class="btn btn-xl btn-primary">Send email <i
										class="mdi mdi-mail-send"></i></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="email-modal" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
	<div class="modal-dialog custom-width">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" class="close md-close"><span
							class="mdi mdi-close"></span></button>
				<h3 class="modal-title">
					<span class="mdi mdi-edit"></span>
					Add New Email
				</h3>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" data-parsley-validate="" novalidate>
					<input type="hidden" name="action"
					       value="<?php echo $emailActionToken; ?>">
					<input type="hidden" name="case"
					       value="<?php echo $addEmailCaseToken; ?>">
					<input type="hidden" name="id" id="id" value="">

					<div class="form-group">
						<label for="email_subject" class="col-sm-12 ">Subject</label>
						<div class="col-sm-12">
							<input class="form-control" required="" id="email_subject" name="subject"
							       placeholder="Subject">
						</div>
					</div>

					<div class="form-group">
						<label class=" col-sm-12" for="email_message">
							Message
						</label>
						<div class="col-sm-12">
							<textarea class="wyziwyg-message" id="email_message" name="message" cols="10"
							          rows="10"></textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-8">
							<button name="submit" class="btn btn-primary btn-xl w-xs">Add</button>
							<button type="reset" class="btn btn-danger btn-xl m-l-5 w-xs"
							        data-dismiss="modal">Cancel
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	$(function () {
		var emailModal = $("#email-modal");
		var emailSendModal = $("#email-send-modal");
		var body = $("body");
		var emailTable = $(".email-data-table");

		App.formElements();

		$("#cc-tags").select2({
			tags: true,
			multiple: true,
			width: "100%",
			placeholder: 'CC'
		});

		$('#email-select').change(function () {
			if ($(this).val() === "") {
				$('.email-container').hide(600);
			} else if ($(this).val() !== "") {
				$('#<?php echo $dynamicFormId ?>').append(
					'<input type="hidden" name="action" value="<?php echo $emailActionToken; ?>">' +
					'<input type="hidden" name="case" value="<?php echo $singleSendEmailCaseToken; ?>">' +
					'<input type="hidden" name="id" value="' + $(this).val() + '">'
				);
				$('#<?php echo $dynamicFormId ?>').submit();
				$('#<?php echo $dynamicFormId ?>').empty();
			}
		});

		$('.ccToggle').click(function (e) {
			e.preventDefault();
			var flag = 0;
			$('#cc-tags').val().forEach(function (i, j) {
				flag++;
				if (j == 0) {
					$('.cc_container').empty().append(
						'<span class="text-muted">Cc:</span>'
					);
				}
				$('.cc_container').append(
					'<span class="text-muted">' + i + '</span>'
				);
			});
			if (flag) {
				$('.cc_container').show(300);
			}
			$('.carbon-copies').toggle(600);
		});


		$('.ccCancel').click(function () {
			$('#cc-tags').val('').trigger('change');
			$('.cc_container').hide(300);
			$('.carbon-copies').hide(600);
			$('.cc_container').empty();
		});

		$('.email-wyziwyg').summernote({
			height: 300,
			toolbar: [
				['style', ['bold', 'italic', 'underline']],
				['font', ['strikethrough', 'superscript', 'subscript']],
				['fontsize', ['fontsize']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['height', ['height']]
			]
		});

		$(".wyziwyg-message").summernote({
			height: 300,
			toolbar: [
				['style', ['bold', 'italic', 'underline']],
				['font', ['strikethrough', 'superscript', 'subscript']],
				['fontsize', ['fontsize']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['height', ['height']]
			]
		});

		var emailDataTable = emailTable.dataTable({
			language: {
				emptyTable: "Oopps! No emails found... Are you sure you have any?"
			},
			buttons: ["copy", "excel", "pdf", "print"/*, {
				extend: 'colvis',
				text: 'Column visibility',
				//columns: ':gt(0)',
				columnText: function (dt, idx, title) {
					title = (title === '') ? 'Tools' : title;
					return (idx + 1) + ': ' + title;
				}
			}*/],
			lengthMenu: [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
			dom: "<'row be-datatable-header'<'col-sm-4'l><'col-sm-4 text-center'BC><'col-sm-4 text-right'f>><'row be-datatable-body'<'col-sm-12'tr>><'row be-datatable-footer'<'col-sm-4'i><'col-sm-8'p>>",
			order: [],
			columnDefs: [
				{"orderable": false, "targets": [0, -1]},
				{"visible": false, "targets": []}
			],
			fixedHeader: true,
			autoWidth: true,
			//responsive: true,
			stateSave: true
		});

		$('#email-view-modal').on('show.bs.modal', function (e) {
			$('#<?php echo $dynamicFormId; ?>').append('<input type="hidden" name="action" value="<?php echo $emailActionToken; ?>">'
				+ '<input type="hidden" name="case" value="<?php echo $singleViewEmailCaseToken; ?>">'
				+ '<input type="hidden" name="id" value="' + $(e.relatedTarget).data('id') + '">');
			$('#<?php echo $dynamicFormId; ?>').submit();
			$('#<?php echo $dynamicFormId; ?>').empty();
		});

		emailModal.on('show.bs.modal', function (e) {
			emailModal.find('form').get(0).reset();
			switch ($(e.relatedTarget).data('case')) {
				case 'edit':
					emailModal.find('[name="action"]').val('<?php echo $emailActionToken; ?>');
					emailModal.find('[name="case"]').val('<?php echo $editEmailCaseToken; ?>');
					emailModal.find('[name="submit"]').html('Edit');
					emailModal.find('.modal-title').html('<span class="mdi mdi-edit"></span> Edit Email');
					$('#<?php echo $dynamicFormId; ?>').append('<input type="hidden" name="action" value="<?php echo $emailActionToken; ?>">'
						+ '<input type="hidden" name="case" value="<?php echo $singleEditEmailCaseToken; ?>">'
						+ '<input type="hidden" name="id" value="' + $(e.relatedTarget).data('id') + '">');
					$('#<?php echo $dynamicFormId; ?>').submit();
					$('#<?php echo $dynamicFormId; ?>').empty();
					break;
				case 'new':
				default:
					emailModal.find('.modal-title').html('<span class="mdi mdi-edit"></span> Add New Email');
					emailModal.find('[name="action"]').val('<?php echo $emailActionToken; ?>');
					emailModal.find('[name="case"]').val('<?php echo $addEmailCaseToken; ?>');
					emailModal.find('[name="submit"]').html('Add');
					break;
			}

		});

		emailSendModal.on('show.bs.modal', function (e) {
			var emails = [],
				field = $(".tags"),
				idInput = $("#ids");
			$('.recordCheckBox:checked').each(function () {
				emails.push($(this).data("email"));
			});
			field.val(emails).trigger("change");
			idInput.val(emails.join(","));
		});

		body.on('click', 'a.emailTableToolDelete', function (e) {
			e.preventDefault();
			var id = $(this).data('id');
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this email!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				showLoaderOnConfirm: true
			}).then(function () {
				$('#<?php echo $dynamicFormId; ?>').append('<input type="hidden" name="action" value="<?php echo $emailActionToken; ?>">'
					+ '<input type="hidden" name="case" value="<?php echo $deleteEmailCaseToken; ?>">'
					+ '<input type="hidden" name="id" value="' + id + '">');
				$('#<?php echo $dynamicFormId; ?>').submit();
				$('#<?php echo $dynamicFormId; ?>').empty();
			});
		});

	});

</script>
