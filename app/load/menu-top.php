<?php
/*$notifications = NotificationDBManager::please()->getAllNotifications(); $counter = 0;
for($x = 0; $x < count($notifications); $x++) {
    if($x < 3)
        $display_notifications[] = $notifications[$x];

    if($notifications[$x]['status_seen'] == 0)
        $counter++;
}*/
?>
<nav class="navbar navbar-default navbar-fixed-top be-top-header">
	<div class="container-fluid">
		<div class="navbar-header">
            <a href="index" class="navbar-brand">
                <img src="<?php echo Config::DEFAULT_LOGO ?>" alt="logo">
            </a>
        </div>
		<div class="be-left-navbar">
			<ul class="nav navbar-nav be-user-nav">
				<!--<li <?php /*echo isset($page) && $page == 'users' ? 'class="active"' : '';*/?>>
					<a href="users">
						<i class="icon mdi mdi-account"></i>
						<span>Users</span>
					</a>
				</li>-->
				<li <?php echo isset($page) && $page == 'registrations' ? 'class="active"' : '';?>>
					<a href="registrations">
						<i class="icon mdi mdi-email"></i>
						<span>Registrations</span>
					</a>
				</li>
				<li <?php echo isset($page) && $page == 'emails' ? 'class="active"' : '';?>>
					<a href="emails">
						<i class="icon mdi mdi-email"></i>
						<span>Emails</span>
					</a>
				</li>
			</ul>
		</div>
		<div class="be-right-navbar">
			<ul class="nav navbar-nav navbar-right be-user-nav">
				<li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false"
					class="dropdown-toggle"><img src="assets/img/avatar.png" alt="Avatar"><span
					class="user-name"><?php echo $_SESSION['user']['name'] ?></span></a>
					<ul role="menu" class="dropdown-menu">
						<li>
							<div class="user-info">
								<div class="user-name"><?php echo $_SESSION['user']['name'] ?></div>
								<div class="user-position online">Available</div>
							</div>
						</li>
						<li><a href="#" data-toggle="modal" data-target="#user-modal"><span class="icon mdi mdi-face"></span> Account</a></li>
						<li><a href="core/logout"><span class="icon mdi mdi-power"></span> Logout</a></li>
					</ul>
				</li>
			</ul>
			<!-- <div class="page-title"><span><?php #echo ucfirst($page) ?></span></div> -->
			<ul class="nav navbar-nav navbar-right be-icons-nav">
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
                        <span class="icon mdi mdi-notifications"></span>
                        <?php //if($counter > 0){ ?> <span class="indicator"></span> <?php //} ?>
                    </a>
					<ul class="dropdown-menu be-notifications">
					<li>
						<div class="title">Notifications<span class="badge"><?php //echo $counter ?></span></div>
							<div class="list">
								<div class="be-scroller">
									<div class="content">
										<ul>
                                            <?php /*foreach ($display_notifications as $notification) { */?><!--
    											<li class="notification <?php /*echo !$notification['status_seen'] ? "notification-unread" : "" */?>">
                                                    <a href="notifications">
        												<div class="image">
                                                            <img src="<?php /*echo $notification['icon'] */?>" alt="<?php /*echo $notification['title'] */?>">
        												</div>
        												<div class="notification-info">
        													<div class="text">
                                                                <span class="user-name"><?php /*echo $notification['title'] */?></span>
        														<span><?php /*echo $notification['description'] */?></span>
                                                                <span class="date"><?php /*echo date('F d, Y h:i a', strtotime($notification['creation_date'])) */?></span>
        													</div>
        												</div>
    											    </a>
                                                </li>
                                                --><?php
/*                                            } */?>
										</ul>
									</div>
								</div>
							</div>
							<div class="footer"><a href="notifications">View all notifications</a></div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>
<!-- /top navigation -->
<script>
$(function() {
    if($('.be-icons-nav .dropdown span.indicator').length > 0){
        $('.be-icons-nav .dropdown').on({
            'shown.bs.dropdown': function() {
                    $.post('core.php', {
                        action: "<?php echo Tokenizer::add('post-action-notification', 20, 'notification'); ?>",
                        case: "<?php echo Tokenizer::add('post-action-notification-mark', 30, 'mark-notifications')?>"
                    });
            },
            'hide.bs.dropdown': function () {
                $(this).find('span.indicator').remove();
            }
        });
    }
});
</script>
