<div class="be-left-sidebar">
	<div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Dashboard</a>
		<div class="left-sidebar-spacer">
			<div class="left-sidebar-scroll">
				<div class="left-sidebar-content">
					<div class="clock"></div>
					<ul class="sidebar-elements">
						<li class="divider">Menu</li>
						<li <?php echo isset($page) && $page == 'index' ? 'class="active"' : '';?>>
							<a href="index">
								<i class="icon mdi mdi-home"></i>
								<span>Tableau de bord</span>
							</a>
						</li>
                        <li <?php echo isset($page) && $page == 'users' ? 'class="active"' : '';?>>
                            <a href="users">
                                <i class="icon mdi mdi-account"></i>
                                <span>Utilisateurs</span>
                            </a>
                        </li>
                        <li <?php echo isset($page) && $page == 'registrations' ? 'class="active"' : '';?>>
                            <a href="registrations.php">
                                <i class="icon mdi mdi-email"></i>
                                <span>Registrations</span>
                            </a>
                        </li>
                    </ul>
				</div>
			</div>
		</div>
		<div class="progress-widget">
			<div class="progress-data"><span class="progress-value">81%</span><span class="name">Ninjas Deployed</span></div>
			<div class="progress">
				<div style="width: 81%;" class="progress-bar progress-bar-danger"></div>
			</div>
		</div>
		<div class="progress-widget">
			<div class="progress-data"><span class="progress-value">56%</span><span class="name">Beagles Polymorphed</span></div>
			<div class="progress">
				<div style="width: 56%;" class="progress-bar progress-bar-warning"></div>
			</div>
		</div>
	</div>
</div>
