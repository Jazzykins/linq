<?php
//TODO: add ids to the hidden input on cc focus out... meh...
//TODO: check if it actually mails?
if (file_exists("../head.php"))
	include("../head.php");

$dynamicFormId = Tokenizer::get('dynamic-form-id', Tokenizer::GET_TOKEN);
$registrations = RegistrationDBManager::please()->getRegistration();
$emails = EmailDBManager::please()->getAllEmails();

//tokens
$registrationActionToken = Tokenizer::add('post-action-registration', 20, 'registration');
$addRegistrationCaseToken = Tokenizer::add('post-case-add', 30, 'add');
$editRegistrationCaseToken = Tokenizer::add('post-case-edit', 30, 'edit');
$deleteRegistrationCaseToken = Tokenizer::add('post-case-delete', 30, 'delete');
$singleEditRegistrationCaseToken = Tokenizer::add('post-case-single-edit', 30, 'single-edit');
$singleViewRegistrationCaseToken = Tokenizer::add('post-case-single-view', 30, 'single-view');
$commentsRegistrationCaseToken = Tokenizer::add('post-case-comments', 30, 'comments');

$emailActionToken = Tokenizer::add('post-action-email', 20, 'email');
$singleSendEmailCaseToken = Tokenizer::add('post-case-single-send', 30, 'single-send');
$sendEmailCaseToken = Tokenizer::add('post-case-send', 30, 'send');

?>
<div class="panel panel-default panel-border-color panel-border-color-primary">
	<div class="panel-heading text-right">
		<a class="btn btn-primary btn-xl" href="#registration-modal" data-case="new" data-toggle="modal">
			New Registration <i class="mdi mdi-plus"></i>
		</a>
		<a class="btn btn-primary btn-xl" id="send-email-btn" href="#email-send-modal" data-case="new"
		   data-toggle="modal">
			Send Email <i class="mdi mdi-mail-send"></i>
		</a>
		<div class="clearfix"></div>
	</div>
	<div class="panel-body">
		<div class="p-20">
			<div id="registrations">
				<h2>Registrations</h2>
				<table class="table data-table table-striped table-hover table-fw-widget">
					<thead>
					<tr>
						<th>
							<div class="be-checkbox be-checkbox-color inline">
								<input id="selectAll" data-toggle="tooltip" data-placement="right"
								       title="Select All" type="checkbox" class="styledCheckbox">
								<label for="selectAll"></label>
							</div>
						</th>
						<th>Name</th>
						<th>Phone</th>
						<th>Email</th>
						<th>Notes</th>
						<th>Buying Frame</th>
						<th># of bedrooms</th>
						<th>Current Residence</th>
						<th>Message</th>
						<th><i class="mdi mdi-wrench"></i></th>
					</tr>
					</thead>
					<tbody>
					<?php

					$total = 0;
					$emailList = array();
					foreach ($registrations as $value) {

						if (true) {
							$emailList[] = $value['registration_email'];
							$total++;
							?>
							<tr>
								<td>
									<div class="be-checkbox be-checkbox-color inline">
										<input id="<?php echo $value['registration_id'] ?>" type="checkbox"
										       class="styledCheckbox recordCheckBox"
										       data-email="<?php echo $value['registration_email'] ?>"
										       value="<?php echo $value['registration_id'] ?>">
										<label for="<?php echo $value['registration_id'] ?>"></label>
									</div>
								</td>
								<td>
									<a href="registration/<?php echo IDObfuscator::encode($value['registration_id']) ?>"><?php echo $value['registration_name'] ?></a>
								</td>
								<td><?php echo $value['registration_phone'] ?></td>
								<td><?php echo $value['registration_email'] ?></td>
								<td>
									<form>
												<textarea title="comments" class="form-control" name="comments"
												          data-id="<?php echo $value['registration_id'] ?>"><?php echo $value['registration_comments'] ?></textarea>
									</form>
								</td>
								<td><?php echo $value['registration_buying_frame'] ?></td>
								<td><?php echo $value['registration_bedrooms'] ?></td>
								<td><?php echo $value['registration_residence'] ?></td>
								<td><?php echo $value['registration_message'] ?></td>
								<td class="actions">
									<a href="#view-modal" data-toggle="modal"
									   title="View Info"
									   data-id="<?php echo $value['registration_id'] ?>"
									   class="icon tableToolInfo">
										<i class="mdi mdi-info"></i>
									</a>
									<a href="#registration-modal" data-toggle="modal"
									   title="Edit"
									   data-id="<?php echo $value['registration_id'] ?>"
									   class="icon tableToolEdit" data-case="edit">
										<i class="mdi mdi-edit"></i>
									</a>
									<a href="#" data-id="<?php echo $value['registration_id'] ?>"
									   title="Delete"
									   class="icon tableToolDelete">
										<i class="mdi mdi-delete"></i>
									</a>
								</td>
							</tr>
							<?php
						}
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
		<p class="text-muted font-13">
			Total of
			<span class="users-count">
				<?php echo $total; ?>Registration<?php echo $total != 1 ? 's' : '' ?>
			</span>
		</p>
	</div>
</div>

<div id="view-modal" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
	<div class="modal-dialog custom-width">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" class="close md-close"><span
							class="mdi mdi-close"></span></button>
				<h3 class="modal-title"><span class="mdi mdi-info-outline"></span> Registration Information</h3>
			</div>
			<div class="modal-body">
				<table class="table table-striped table-hover ">
					<tr>
						<th>Name</th>
						<td data-name=""></td>
					</tr>
					<tr>
						<th>Phone</th>
						<td data-phone=""></td>
					</tr>
					<tr>
						<th>Email</th>
						<td data-email=""></td>
					</tr>
					<tr>
						<th>Buying frame</th>
						<td data-buying_frame=""></td>
					</tr>
					<tr>
						<th>Number of Bedrooms</th>
						<td data-bedrooms=""></td>
					</tr>
					<tr>
						<th>Current residence</th>
						<td data-residence=""></td>
					</tr>
					<tr>
						<th>Message</th>
						<td data-message=""></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="email-send-modal" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
	<div class="modal-dialog custom-width" style="max-width: 100%;width: 1224px;">
		<div class="modal-content" style="max-width: 90%;width: 100%;">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" class="close md-close"><span
							class="mdi mdi-close"></span></button>
				<h3 class="modal-title"><span class="mdi mdi-info-outline"></span> Registration Information</h3>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" data-parsley-validate="" novalidate>
					<input type="hidden" name="action" value="<?php echo $emailActionToken ?>">
					<input type="hidden" name="case" value="<?php echo $sendEmailCaseToken ?>">
					<input type="hidden" name="ids" id="ids" value="">
					<div class="main-content container-fluid">
						<div class="col-sm-6">
							<label for="email-select">Choose an email</label>
							<select class="select2" name="email_id" id="email-select">
								<option value="">---</option>
								<?php foreach ($emails as $email): ?>
									<option value="<?php echo $email['email_id'] ?>"><?php echo $email['email_subject'] ?></option>
								<?php endforeach; ?>
								<option value="blank">New email</option>
							</select>
						</div>
					</div>
					<div class="main-content container-fluid email-container" style="display: none;">
						<hr>
						<div class="email-head">
							<div class="email-head-subject">
								<div class="title">
									<span id="email-title"></span>
								</div>
							</div>
							<div class="email-head-sender">
								<div class="date"><?php echo Functions::userFriendlyDate(date(time())); ?></div>
								<div class="sender">
									Receivers:
									<div class="actions">
										<a href="#" class="ccToggle"><i class="mdi mdi-caret-down"></i></a>
									</div>
									<div class="cc_container"></div>
									<div class="row carbon-copies">
										<div class="col-sm-8">
											<select name="carbon_copies[]" class="form-control input-sm tags"
											        id="cc-tags" multiple>
												<?php
												foreach ($emailList as $email)
													echo "<option value='$email'>$email</option>";
												?>
											</select>
										</div>
										<div class="col-sm-2">
											<button type="button" class="btn btn-primary ccToggle"><i
														class="glyphicon glyphicon-ok"></i></button>
											<button type="button" class="btn btn-default ccCancel"><i
														class="glyphicon glyphicon-remove"></i></button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="email-body"></div>
						<div class="col-sm-12">
							<hr>
							<button type="submit" class="btn btn-primary btn-xl">Send <i class="mdi mdi-mail-send"></i>
							</button>
						</div>
					</div>
					<div class="main-content container-fluid blank-email-container" style="display: none;">
						<div class="form-group">
							<label for="">Subject</label>
							<input type="text" class="form-control" name="subject" placeholder="" id="subject-blank">
						</div>
						<div class="form-group">
							<label for="cc-email-blank">CC</label>
							<select name="carbon_copies[]" class="tags" multiple>
								<?php
								foreach ($emailList as $email)
									echo "<option value='$email'>$email</option>";
								?>
							</select>
						</div>
						<div class="form-group">
							<label for="email-wyziwyg">Message</label>
							<textarea name="message" class="email-wyziwyg"></textarea>
						</div>
						<div class="form-group pull-right" style="padding-top: 22px">
							<button type="submit" name="send_email" class="btn btn-xl btn-primary">Send email <i
										class="mdi mdi-mail-send"></i></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="registration-modal" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
	<div class="modal-dialog custom-width">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" class="close md-close"><span
							class="mdi mdi-close"></span></button>
				<h3 class="modal-title">
					<span class="mdi mdi-edit"></span>
					Add New Registration
				</h3>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" data-parsley-validate="" novalidate>
					<input type="hidden" name="action"
					       value="<?php echo $registrationActionToken; ?>">
					<input type="hidden" name="case"
					       value="<?php echo $addRegistrationCaseToken; ?>">
					<input type="hidden" name="id" id="id" value="">

					<div class="form-group">
						<label for="name" class="col-sm-4 control-label">Name</label>
						<div class="col-sm-8">
							<input class="form-control" required="" id="name" name="name"
							       placeholder="Name">
						</div>
					</div>

					<div class="form-group">
						<label for="phone" class="col-sm-4 control-label">Phone</label>
						<div class="col-sm-8">
							<input class="form-control" required="" id="phone" name="phone"
							       placeholder="Phone">
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="col-sm-4 control-label">Email</label>
						<div class="col-sm-8">
							<input class="form-control" required="" id="email" name="email"
							       placeholder="Email">
						</div>
					</div>

					<div class="form-group">
						<label for="buying_frame" class="col-sm-4 control-label">Buying frame</label>
						<div class="col-sm-8">
							<select class="form-control" name="buying_frame" id="buying_frame">
								<option value="now">Now</option>
								<option value="3 months">3 months</option>
								<option value="6 months">6 months</option>
								<option value="12 months+">12 months or more</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="name" class="col-sm-4 control-label">Number of bedrooms</label>
						<div class="col-sm-8">
							<div class="be-radio">
								<input id="2bedrooms" class=" radio-style" name="bedrooms" value="2 bedrooms"
								       type="radio" checked>
								<label for="2bedrooms" class="radio-style-1-label radio-small">2 Bedrooms</label>
							</div>
							<div class="be-radio">
								<input id="3bedrooms" class="  radio-style" name="bedrooms" value="3 bedrooms"
								       type="radio">
								<label for="3bedrooms" class="radio-style-2-label radio-small">3 Bedrooms</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label" for="residence">Current Residence</label>
						<div class="col-sm-8">
							<div class="be-radio">
								<input id="vaudreuil_form" class=" radio-style" name="residence" value="Vaudreuil"
								       type="radio" checked>
								<label for="vaudreuil_form"
								       class="radio-style-1-label radio-small">Vaudreuil</label>
							</div>
							<div class="be-radio">
								<input id="westIsland_form" class=" radio-style" name="residence"
								       value="West Island" type="radio">
								<label for="westIsland_form" class="radio-style-2-label radio-small">West
									Island</label>
							</div>
							<div class="be-radio">
								<input id="otherResidence_form" class="radio-style" value="Other"
								       name="residence" type="radio">
								<label for="otherResidence_form"
								       class="radio-style-2-label radio-small">Other</label>
							</div>
						</div>

					</div>

					<div class="form-group">
						<label class="control-label col-sm-4" for="message">
							Message
						</label>
						<div class="col-sm-8">
                                        <textarea class="form-control" id="message" name="message" cols="10"
                                                  rows="10"></textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-8">
							<button name="submit" class="btn btn-primary btn-xl w-xs">Add</button>
							<button type="reset" class="btn btn-danger btn-xl m-l-5 w-xs"
							        data-dismiss="modal">Cancel
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	$(function () {
		var modal = $("#registration-modal");
		var body = $("body");
		var registrationTable = $(".data-table");
		var emailSendModal = $("#email-send-modal");

		App.formElements();

		$("#cc-tags").select2({
			tags: true,
			multiple: true,
			width: "100%",
			placeholder: 'CC'
		});

		$('#email-select').change(function () {
			if ($(this).val() === "") {
				$('.email-container').hide(600);
			} else if ($(this).val() !== "") {
				$('#<?php echo $dynamicFormId ?>').append(
					'<input type="hidden" name="action" value="<?php echo $emailActionToken; ?>">' +
					'<input type="hidden" name="case" value="<?php echo $singleSendEmailCaseToken; ?>">' +
					'<input type="hidden" name="id" value="' + $(this).val() + '">'
				);
				$('#<?php echo $dynamicFormId ?>').submit();
				$('#<?php echo $dynamicFormId ?>').empty();
			}
		});

		$('.ccToggle').click(function (e) {
			e.preventDefault();
			var flag = 0;
			$('#cc-tags').val().forEach(function (i, j) {
				flag++;
				if (j == 0) {
					$('.cc_container').empty().append(
						'<span class="text-muted">Cc:</span>'
					);
				}
				$('.cc_container').append(
					'<span class="text-muted">' + i + '</span>'
				);
			});
			if (flag) {
				$('.cc_container').show(300);
			}
			$('.carbon-copies').toggle(600);
		});


		$('.ccCancel').click(function () {
			$('#cc-tags').val('').trigger('change');
			$('.cc_container').hide(300);
			$('.carbon-copies').hide(600);
			$('.cc_container').empty();
		});

		$('.email-wyziwyg').summernote({
			height: 300,
			toolbar: [
				['style', ['bold', 'italic', 'underline']],
				['font', ['strikethrough', 'superscript', 'subscript']],
				['fontsize', ['fontsize']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['height', ['height']]
			]
		});

		body.on('change', '#selectAll', function (e) {
			var table = $(e.target).closest('table');
			$('input:checkbox', table).prop('checked', this.checked);
		});

		var registrationDataTable = registrationTable.dataTable({
			language: {
				emptyTable: "Oopps! No emails found... Are you sure you have any?"
			},
			buttons: ["copy", "excel", "pdf", "print", {
				extend: 'colvis',
				text: 'Column visibility',
				//columns: ':gt(0)',
				columnText: function (dt, idx, title) {
					title = (title === '') ? 'Tools' : title;
					return (idx + 1) + ': ' + title;
				}
			}],
			lengthMenu: [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
			dom: "<'row be-datatable-header'<'col-sm-4'l><'col-sm-4 text-center'BC><'col-sm-4 text-right'f>><'row be-datatable-body'<'col-sm-12'tr>><'row be-datatable-footer'<'col-sm-4'i><'col-sm-8'p>>",
			order: [],
			columnDefs: [
				{"orderable": false, "targets": [0, -1]},
				{"visible": false, "targets": [-2, -3, -4, -5]}
			],
			fixedHeader: true,
			autoWidth: true,
			//responsive: true,
			stateSave: true
		});

		$('#view-modal').on('show.bs.modal', function (e) {
			$('#<?php echo $dynamicFormId; ?>').append('<input type="hidden" name="action" value="<?php echo $registrationActionToken; ?>">'
				+ '<input type="hidden" name="case" value="<?php echo $singleViewRegistrationCaseToken; ?>">'
				+ '<input type="hidden" name="id" value="' + $(e.relatedTarget).data('id') + '">');
			$('#<?php echo $dynamicFormId; ?>').submit();
			$('#<?php echo $dynamicFormId; ?>').empty();
		});

		emailSendModal.on('show.bs.modal', function (e) {
			var emails = [],
				field = $(".tags"),
				idInput = $("#ids");
			$('.recordCheckBox:checked').each(function () {
				emails.push($(this).data("email"));
			});
			field.val(emails).trigger("change");
			idInput.val(emails.join(","));
		});

		modal.on('show.bs.modal', function (e) {
			modal.find('form').get(0).reset();
			switch ($(e.relatedTarget).data('case')) {
				case 'edit':
					modal.find('[name="action"]').val('<?php echo $registrationActionToken; ?>');
					modal.find('[name="case"]').val('<?php echo $editRegistrationCaseToken; ?>');
					modal.find('[name="submit"]').html('Edit');
					modal.find('.modal-title').html('<span class="mdi mdi-edit"></span> Edit Registration');
					$('#<?php echo $dynamicFormId; ?>').append('<input type="hidden" name="action" value="<?php echo $registrationActionToken; ?>">'
						+ '<input type="hidden" name="case" value="<?php echo $singleEditRegistrationCaseToken; ?>">'
						+ '<input type="hidden" name="id" value="' + $(e.relatedTarget).data('id') + '">');
					$('#<?php echo $dynamicFormId; ?>').submit();
					$('#<?php echo $dynamicFormId; ?>').empty();
					break;
				case 'new':
				default:
					modal.find('.modal-title').html('<span class="mdi mdi-edit"></span> Add New Registration');
					modal.find('[name="action"]').val('<?php echo $registrationActionToken; ?>');
					modal.find('[name="case"]').val('<?php echo $addRegistrationCaseToken; ?>');
					modal.find('[name="submit"]').html('Add');
					break;
			}

		});

		body.on('click', 'a.tableToolDelete', function (e) {
			e.preventDefault();
			var id = $(this).data('id');
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this registration!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				showLoaderOnConfirm: true
			}).then(function () {
				$('#<?php echo $dynamicFormId; ?>').append('<input type="hidden" name="action" value="<?php echo $registrationActionToken; ?>">'
					+ '<input type="hidden" name="case" value="<?php echo $deleteRegistrationCaseToken; ?>">'
					+ '<input type="hidden" name="id" value="' + id + '">');
				$('#<?php echo $dynamicFormId; ?>').submit();
				$('#<?php echo $dynamicFormId; ?>').empty();
			});
		});

		body.on('blur', 'textarea[name="comments"]', function () {
			$('#<?php echo $dynamicFormId; ?>').append('<input type="hidden" name="action" value="<?php echo $registrationActionToken; ?>">'
				+ '<input type="hidden" name="case" value="<?php echo $commentsRegistrationCaseToken; ?>">'
				+ '<input type="hidden" name="id" value="' + $(this).data('id') + '">'
				+ '<textarea type="hidden" name="comments">' + $(this).val() + '</textarea>');
			$('#<?php echo $dynamicFormId; ?>').submit();
			$('#<?php echo $dynamicFormId; ?>').empty();
		});

	});

</script>
