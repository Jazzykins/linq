<?php
if (file_exists("../head.php"))
include("../head.php");
?>

<div class="panel panel-default panel-border-color panel-border-color-primary">
    <div class="panel-heading">
        <button class="btn btn-primary btn-xl pull-right" data-toggle="modal"
        data-target="#add-modal">New User <i class="mdi mdi-account-add"></i></button>
        <h2 class="pull-left">Users</h2>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">
        <div class="p-20">
            <table class="table table-striped m-0" id="datatable-editable">
                <thead>
                    <tr>
                        <th width="25%">Name</th>
                        <th width="15%">Username</th>
                        <th width="20%">Email</th>
                        <th width="10%">Level</th>
                        <th width="20%">Last Login</th>
                        <th width="10%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $total = 0;
                    foreach (UserDBManager::please()->getUsers() as $value) {
                        if (true) {//$value['user_id'] != $_SESSION['user']['user_id'] && $_SESSION['user']['level'] > $value['level']){
                            $total++;
                            ?>
                                <tr>
                                    <td><?php echo $value['name'] ?></td>
                                    <td><?php echo $value['username'] ?></td>
                                    <td><?php echo $value['email'] ?></td>
                                    <td><span class="label label-default"><?php echo ucfirst(User::getUserLevel($value['level'])); ?></span></td>
                                    <td><?php echo Functions::userFriendlyDate($value['last_login']); ?></td>
                                    <td class="actions">
                                        <a href="#" data-toggle="modal" data-target="#edit-modal"
                                        data-id="<?php echo $value['user_id'] ?>" title="Edit User"
                                        class="on-default label label-warning edit-row"><i class="mdi mdi-edit"></i></a>
                                        <a href="#" data-id="<?php echo $value['user_id'] ?>" title="Delete User"
                                            class="on-default label label-danger remove-row"><i class="mdi mdi-delete"></i></a>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                    if ($total == 0)
                    echo "<tr><td colspan='6' class='text-center'><i>oupps, no user found. Are you sure you have any?</i></td></tr>" ?>
                </tbody>
            </table>
        </div>
        <p class="text-muted font-13">
            Total of <span class="users-count"><?php echo $total; ?> User<?php echo $total > 1 ? 's' : '' ?></span>
        </p>
    </div>
</div>

<div id="add-modal" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
                <h3 class="modal-title">Add New User</h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" data-parsley-validate="" novalidate>
                    <input type="hidden" name="action"
                    value="<?php echo Tokenizer::add('post-action-user', 20, 'user'); ?>">
                    <input type="hidden" name="case"
                    value="<?php echo Tokenizer::add('post-action-user-add', 30, 'add'); ?>">

                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">Name</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" required=""
                            data-parsley-length="[<?php echo User::MIN_NAME_LENGTH . "," . User::MAX_NAME_LENGTH ?>]"
                            id="name" name="name" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-sm-4 control-label">Username</label>
                        <div class="col-sm-7">
                            <input type="text" id="username" name="username" class="form-control" required=""
                            data-parsley-length="[<?php echo User::MIN_USERNAME_LENGTH . "," . User::MAX_USERNAME_LENGTH ?>]"
                            placeholder="Username">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-4 control-label">Email</label>
                        <div class="col-sm-7">
                            <input type="email" id="email" name="email" class="form-control" required=""
                            parsley-type="email"
                            data-parsley-length="[<?php echo User::MIN_EMAIL_LENGTH . "," . User::MAX_EMAIL_LENGTH ?>]"
                            placeholder="Enter a valid e-mail">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-4 control-label">Password</label>
                        <div class="col-sm-6">
                            <input type="text" id="password" name="password"
                            data-parsley-length="[<?php echo User::MIN_PASSWORD_LENGTH . "," . User::MAX_PASSWORD_LENGTH ?>]"
                            placeholder="Password" required class="form-control" value="">
                        </div>
                        <div class="col-sm-1 p-t-10">
                            <button class="btn btn-info btn-xs" title="click to generate password" id="genBtn"><i
                                class="mdi mdi-refresh"></i></button>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">User Level</label>
                            <div class="col-sm-7">
                                <select class="form-control" name="level">
                                    <option value="<?php echo User::LEVEL_GOD_ADMIN; ?>">God</option>
                                    <option value="<?php echo User::LEVEL_ADMIN; ?>">Administrator</option>
                                    <option value="<?php echo User::LEVEL_USER; ?>" selected>Standard</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <div class="be-checkbox">
                                    <input id="emailUser" class="flat" type="checkbox" name="emailUser">
                                    <label for="emailUser"> Email User his credentials </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-primary btn-xl w-xs">Add</button>
                                <button type="reset" class="btn btn-danger btn-xl m-l-5 w-xs"
                                data-dismiss="modal">Cancel
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="edit-modal" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
                <h3 class="modal-title">Edit User</h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" data-parsley-validate="" novalidate>
                    <input type="hidden" name="action"
                    value="<?php echo Tokenizer::add('post-action-user', 20, 'user'); ?>">
                    <input type="hidden" name="case"
                    value="<?php echo Tokenizer::add('post-action-user-edit', 30, 'edit'); ?>">
                    <input type="hidden" name="user_id" value="">


                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">Name</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" required=""
                            data-parsley-length="[<?php echo User::MIN_NAME_LENGTH . "," . User::MAX_NAME_LENGTH ?>]"
                            id="name" name="name" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-sm-4 control-label">Username</label>
                        <div class="col-sm-7">
                            <input type="text" id="username" name="username" class="form-control" required=""
                            data-parsley-length="[<?php echo User::MIN_USERNAME_LENGTH . "," . User::MAX_USERNAME_LENGTH ?>]"
                            placeholder="Username">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-4 control-label">Email</label>
                        <div class="col-sm-7">
                            <input type="email" id="email" name="email" class="form-control" required=""
                            parsley-type="email"
                            data-parsley-length="[<?php echo User::MIN_EMAIL_LENGTH . "," . User::MAX_EMAIL_LENGTH ?>]"
                            placeholder="Enter a valid e-mail">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">User Level</label>
                        <div class="col-sm-7">
                            <select class="form-control" name="level">
                                <option value="<?php echo User::LEVEL_GOD_ADMIN; ?>">God</option>
                                <option value="<?php echo User::LEVEL_ADMIN; ?>">Administrator</option>
                                <option value="<?php echo User::LEVEL_USER; ?>" selected>Standard</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <div class="be-checkbox">
                                <input id="deleteUser" type="checkbox" name="deleteUser">
                                <label for="deleteUser"> Delete User </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" class="btn btn-primary btn-xl">Save</button>
                            <button type="reset" class="btn btn-danger btn-xl m-l-5"
                            data-dismiss="modal">Cancel
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>

    <script>
    $('#genBtn').on('click', function (e) {
        e.preventDefault();
        $('#password').val(generateRandomString(generateRandomNumber(<?php echo User::MIN_PASSWORD_LENGTH . "," . User::MAX_PASSWORD_LENGTH ?>), true, false, true, false));
        return false;
    });

    $('#edit-modal').on('show.bs.modal', function (e) {
        $.ajax({
            url: 'core.php',
            type: 'POST',
            dataType: 'json',
            data: {
                'action': "<?php echo Tokenizer::add('post-action-user', 20, 'user'); ?>",
                'case': "<?php echo Tokenizer::add('post-action-user-single', 30, 'single'); ?>",
                'id': $(e.relatedTarget).data('id')
            },
            success: function (data) {
                $(e.currentTarget).find('input[name="user_id"]').val(data['user']['user_id']);
                $(e.currentTarget).find('input[name="name"]').val(data['user']['name']);
                $(e.currentTarget).find('input[name="username"]').val(data['user']['username']);
                $(e.currentTarget).find('input[name="email"]').val(data['user']['email']);
                $(e.currentTarget).find('select[name="level"]').val(data['user']['level']);
            },
            error: function (xhr, textStatus, errorThrown) {
                $('#edit-modal').modal('hide');
                generateNotification("That's bad!! Something is definitely wrong", "bottom-right", "error", 5000);
                console.log(xhr.responseText);
            }
        });
    });

    $('a.remove-row').on('click', function (e) {
        e.preventDefault();
        var tr = $(this).parent().parent(),
        id = $(this).attr('data-id');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this user!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url: 'core.php',
                type: 'POST',
                dataType: 'json',
                data: {
                    'action': "<?php echo Tokenizer::add('post-action-user', 20, 'user'); ?>",
                    'case': "<?php echo Tokenizer::add('post-action-user-delete', 30, 'delete'); ?>",
                    'user_id': id
                },
                success: function (data) {
                    if (data['delete']) {
                        swal("Deleted!", data['username'] + " has been deleted.", "success");
                        executeAction("delete-user");
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    generateNotification("That's bad!! Something is definitely wrong", "bottom-right", "error", 5000);
                }
            });

        });

        return false;
    });
    </script>
