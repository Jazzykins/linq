<?php
	# Function to auload an object class
	//require_once 'models/vendor/autoload.php';
	require_once 'models/vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

	// Or, using an anonymous function as of PHP 5.3.0
	spl_autoload_register(function ($class) {
		require_once("models/$class.class.php");
	});

    # Starting the session
    ob_start();
	if(session_status() == PHP_SESSION_NONE)
        session_start();

    # Displaying the errors
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');

    #set default timezone
    date_default_timezone_set('America/Montreal');

    # Creating the db and page variables
    $page = rtrim(basename($_SERVER['PHP_SELF']), ".php");
    $db = DBManager::Instance();

    # Check if the user has the right to view the page
    if($page != "core"){
        if(!isset($_SESSION['user'])){
            if(isset($_COOKIE['remember_me'])){
                $key = UserDBManager::please()->getUserKey($_COOKIE['remember_me'], 'AUTO_LOGIN');
                if($key){
                    $user = UserDBManager::please()->reloadProfile($key['user_fk']);

                    if($user){
                        $_SESSION['user'] = $user;
                        setcookie("remember_me", $_COOKIE['remember_me'], time() + 7 * 24 * 60 * 60, '/');
                        UserDBManager::please()->updateUserKeyUsage($key['user_key_id']);
                    }
                }else
                    setcookie("remember_me", "", time() - 54, "/");
            }

            if(!isset($_SESSION['user']) && !Functions::isPublicPage($page)){
                $_SESSION['destination'] = $page;
                Functions::generateErrorMessage("Please log in to continue.");
                Functions::redirect("login");
            }
        }else{
            # Reload the user's profile on page refresh
            $_SESSION['user'] = UserDBManager::please()->reloadProfile($_SESSION['user']['user_id']);

            if($page != 'create-password' && !$_SESSION['user']['changed_password'])
                Functions::redirect("create-password");
    	}
    }
?>
