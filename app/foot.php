</div>

<!-- Includes the modal for user's informarion on each page -->
<?php require_once 'user-modal.php' ?>

<script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/lib/dropzone/dist/dropzone.js" type="text/javascript"></script>
<script src="assets/lib/jquery-flot/jquery.flot.js" type="text/javascript"></script>
<script src="assets/lib/jquery-flot/jquery.flot.pie.js" type="text/javascript"></script>
<script src="assets/lib/jquery-flot/jquery.flot.resize.js" type="text/javascript"></script>
<script src="assets/lib/jquery-flot/plugins/jquery.flot.orderBars.js" type="text/javascript"></script>
<script src="assets/lib/jquery-flot/plugins/curvedLines.js" type="text/javascript"></script>
<script src="assets/lib/jquery.sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="assets/lib/countup/countUp.min.js" type="text/javascript"></script>
<script src="assets/lib/jqvmap/jquery.vmap.min.js" type="text/javascript"></script>
<script src="assets/lib/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="assets/lib/flipclock/flipclock.min.js" type="text/javascript"></script>
<script src="assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script src="assets/lib/parsley/parsley.min.js" type="text/javascript"></script>
<script src="assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="assets/lib/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script src="assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="assets/lib/daterangepicker/js/daterangepicker.js" type="text/javascript"></script>
<script src="assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
<script src="assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
<script src="assets/lib/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="assets/lib/markdown-js/markdown.js" type="text/javascript"></script>
<script src="assets/lib/jquery.fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="assets/lib/summernote/summernote.min.js" type="text/javascript"></script>
<script src="assets/lib/summernote/summernote-ext-beagle.js" type="text/javascript"></script>
<script src="assets/js/main.js" type="text/javascript"></script>

<script src="assets/lib/datatables/datatables.min.js" type="text/javascript"></script>

<script src="assets/js/core.js"></script>
<script src="assets/js/custom.js"></script>

<?php require_once('load/misc/cookie-success-message.php'); ?>
<?php require_once('load/misc/cookie-error-message.php'); ?>

<script type="text/javascript">
	$(document).ready(function(){
		//initialize the javascript
		App.init();
		<?php
        if($page == "index"){ ?>
		    App.dashboard();
		    <?php
        } ?>
	});
</script>
</body>
</html>
