<?php require_once('header.php'); ?>
<?php require_once('load/menu-top.php'); ?>
<?php //require_once('load/menu-left.php'); ?>
<?php require_once('load/misc/dynamic-form.php'); ?>

<div class="be-contentBAK">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div id="registrations-wrapper">
                    <?php require_once('load/registrations.php'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('load/menu-right.php'); ?>
<?php require_once('footer.php'); ?>
<?php require_once('foot.php'); ?>
