<?php require_once('head.php'); ?>

	<!DOCTYPE html>
	<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" href="assets/img/logo-fav.png">
		<title>Beagle</title>
		<link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
		<link rel="stylesheet" type="text/css"
		      href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" href="assets/css/style.css" type="text/css"/>

		<script src="assets/lib/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
		<script src="assets/lib/jquery-cookie/jquery.cookie.js"></script>
	</head>
<body class="be-splash-screen">
<?php
if (User::isLoggedIn()) {
	Functions::generateErrorMessage("You're already logged in.");
	Functions::redirect("main");
}
?>
<div class="be-wrapper be-login">
	<div class="be-content">
		<div class="main-content container-fluid">
			<div class="splash-container">
				<div class="panel panel-default panel-border-color panel-border-color-danger">
					<div class="panel-heading"><img src="assets/img/logo-xx.png" alt="logo" width="102" height="27"
					                                class="logo-img"><span class="splash-description">Please enter your user information.</span>
					</div>
					<div class="panel-body">
						<form class="form-horizontal m-t-20" data-parsley-validate="" novalidate>
							<div role="alert" class="alert alert-primary alert-icon alert-dismissible">
								<div class="icon"><span class="mdi mdi-info-outline"></span></div>
								<div class="message">
									<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span
												aria-hidden="true" class="mdi mdi-close"></span></button>
									You must update your password before you can continue.
								</div>
							</div>
							<input type="hidden" name="action"
							       value="<?php echo Tokenizer::add('post-action-user', 20, 'user'); ?>">
							<input type="hidden" name="version"
							       value="<?php echo Tokenizer::add('post-version-create-password', 10, '1.0.0'); ?>">
							<input type="hidden" name="case"
							       value="<?php echo Tokenizer::add('post-case-create-password', 20, 'create-password'); ?>">
							<div class="form-group ">
								<div class="col-xs-12">
									<input class="form-control" type="password" required="" placeholder="Password"
									       name="password" autocomplete="new-password"
									       data-parsley-length="[<?php echo User::MIN_PASSWORD_LENGTH . "," . User::MAX_PASSWORD_LENGTH ?>]">
								</div>
							</div>

							<div class="form-group">
								<div class="col-xs-12">
									<input class="form-control" type="password" required=""
									       placeholder="Confirm Password"
									       name="cpassword" autocomplete="new-password"
									       data-parsley-length="[<?php echo User::MIN_PASSWORD_LENGTH . "," . User::MAX_PASSWORD_LENGTH ?>]">
								</div>
							</div>

							<div class="form-group text-center m-t-40">
								<div class="col-xs-12">
									<button class="btn btn-block text-uppercase btn-danger btn-xl"
									        type="submit">Save
									</button>
								</div>
							</div>
						</form>

					</div>

				</div>
			</div>
		</div>
	</div>

<?php require_once('foot.php'); ?>