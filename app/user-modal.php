<div id="user-modal" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
	<div class="modal-dialog custom-width">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
				<h2 class="modal-title"><?php echo $_SESSION['user']['name'] ?>
                <span class="badge"><?php echo ucfirst(User::getUserLevel($_SESSION['user']['level'])) ?></span>
            </h2>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" data-parsley-validate="" novalidate>
					<input type="hidden" name="action"
					       value="<?php echo Tokenizer::add('post-action-user', 20, 'user'); ?>">
					<input type="hidden" name="case"
					       value="<?php echo Tokenizer::add('post-action-user-edit', 30, 'edit'); ?>">
					<input type="hidden" name="user_id" value="<?php echo $_SESSION['user']['user_id']?>">
                    <input type="hidden" name="level" value="<?php echo $_SESSION['user']['level']?>">
					<div class="form-group">
						<label for="username" class="col-sm-4 control-label">Nom d'utilisateur</label>
						<div class="col-sm-7">
							<input type="text" id="username" name="username" class="form-control" required=""
							       data-parsley-length="[<?php echo User::MIN_USERNAME_LENGTH . "," . User::MAX_USERNAME_LENGTH ?>]"
							       placeholder="Nom d'utilisateur" value="<?php echo $_SESSION['user']['username'] ?>" readonly>
						</div>
					</div>
                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">Nom</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" required=""
                            data-parsley-length="[<?php echo User::MIN_NAME_LENGTH . "," . User::MAX_NAME_LENGTH ?>]"
                            id="name" name="name" placeholder="Nom" value="<?php echo $_SESSION['user']['name'] ?>">
                        </div>
                    </div>
					<div class="form-group">
						<label for="email" class="col-sm-4 control-label">Email</label>
						<div class="col-sm-7">
							<input type="email" id="email" name="email" class="form-control" required="" parsley-type="email" data-parsley-length="[<?php echo User::MIN_EMAIL_LENGTH . "," . User::MAX_EMAIL_LENGTH ?>]" placeholder="Entrez un email valide" value="<?php echo $_SESSION['user']['email'] ?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-8">
							<button type="submit" class="btn btn-primary btn-xl">Save</button>
							<button type="reset" class="btn btn-danger btn-xl m-l-5"
							        data-dismiss="modal">Cancel
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
