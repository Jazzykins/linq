<?php
$db = new RegistrationDBManager();

$rules = array(
    'name' => 'required|max_len,150|min_len,4',
    'phone' => 'required|max_len,150|min_len,4',
    'email' => 'required|max_len,150|min_len,4',
    'buying_frame' => 'required|max_len,150|min_len,2',
    'bedrooms' => 'required|max_len,150|min_len,4',
    'residence' => 'required|max_len,150|min_len,4',
    'message' => 'max_len,500');

$filters = array(
    'name' => 'trim|sanitize_string',
    'phone' => 'trim|sanitize_string',
    'email' => 'trim|sanitize_string',
    'buying_frame' => 'trim|sanitize_string',
    'bedrooms' => 'trim|sanitize_string',
    'residence' => 'trim|sanitize_string',
    'message' => 'trim|sanitize_string');

switch ($_POST['case']) {
    case "single":
        $resultObj['no-message'] = true;
        $resultObj['registration'] = $db->getSingleRegistration($_POST['registration_id']);
        break;


    case 'add':
        if (Functions::validateArray($_POST, $rules, $filters, $resultObj['error'])) {
            if ($db->addRegistration($_POST)) {
                $resultObj['success'] = "{$_POST['name']} was added successfully.";
                $resultObj['callback'] = "reload-registrations";
                $resultObj['reset'] = true;
                //Tokenizer::delete(array('post-action-email', 'post-case-add'));
            } else
                if (Config::DEBUG_CORE)
                    $resultObj['error'] = Config::DEFAULT_DB_ERROR;
        }
        break;

    case 'single-edit':
        if (isset($_POST['id'])) {
            $email = $db->getSingleRegistration($_POST['id']);
            if ($email) {
                $resultObj['callback-data'] = $email;
                $resultObj['no-message'] = true;
                $resultObj['callback'] = 'get-single-edit-registration';
            } else
                $resultObj['error'] = "Invalid email.";
        } else
            $resultObj['error'] = "Unknown email.";
        break;

    case 'comments':
		$db->addComments($_POST['id'], $_POST['comments']);
        $resultObj['success'] = "Comments saved successfully";
        break;

    case 'single-view':
        if (isset($_POST['id'])) {
            $email = $db->getSingleRegistration($_POST['id']);
            if ($email) {
                $resultObj['callback-data'] = $email;
                $resultObj['no-message'] = true;
                $resultObj['callback'] = 'get-single-registration';
            } else
                $resultObj['error'] = "Invalid email.";
        } else
            $resultObj['error'] = "Unknown email.";
        break;

    case 'edit':
        if (Functions::validateArray($_POST, $rules, $filters, $resultObj['error'])) {
            if ($db->editRegistration($_POST)) {
                $resultObj['success'] = "{$_POST['name']} was edited successfully.";
                $resultObj['callback'] = "reload-registrations";
                $resultObj['reset'] = true;
                //Tokenizer::delete(array('post-action-email', 'post-case-edit'));
            } else
                if (Config::DEBUG_CORE)
                    $resultObj['error'] = Config::DEFAULT_DB_ERROR;
        }
        break;

    case "delete":
        $resultObj['callback'] = "swal";
        $resultObj['no-message'] = true;
        if (isset($_POST['id'])) {
            $email = $db->getSingleRegistration($_POST['id']);
            if ($email) {
                $resultObj['callback-data'] = array("title" => "Deleted!",
                    "message" => $email['registration_name'] . " has been deleted.",
                    "type" => "success",
                    "next-action" => "reload-registrations");
                //Tokenizer::delete(array('post-action-email', 'post-case-delete'));
                $resultObj['delete'] = $db->deleteRegistration($_POST['id']);
            } else
                $resultObj['error'] = "Invalid email.";
        } else
            $resultObj['error'] = "Unknown email.";

        if ($resultObj['error'] != "-1") {
            $resultObj['callback-data'] = array("message" => $resultObj['error'] . " Updating list...",
                "type" => "error",
                "next-action" => "reload-registrations");
        }
        break;
}