<?php
$db = new EmailDBManager();

$rules = array(
	'subject' => 'required|max_len,150|min_len,4',
	'message' => 'required|max_len,500');

$filters = array(
	'subject' => 'trim|sanitize_string',
	'message' => 'trim|sanitize_string');

switch ($_POST['case']) {
	case "single":
		$resultObj['no-message'] = true;
		$resultObj['email'] = $db->getSingleEmail($_POST['email_id']);
		break;


	case 'add':
		if (Functions::validateArray($_POST, $rules, $filters, $resultObj['error'])) {
			if ($db->addEmail($_POST)) {
				$resultObj['success'] = "{$_POST['subject']} was added successfully.";
				$resultObj['callback'] = "reload-registrations";
				$resultObj['reset'] = true;
				//Tokenizer::delete(array('post-action-email', 'post-case-add'));
			} else
				if (Config::DEBUG_CORE)
					$resultObj['error'] = Config::DEFAULT_DB_ERROR;
		}
		break;

	case 'single-edit':
		if (isset($_POST['id'])) {
			$email = $db->getSingleEmail($_POST['id']);
			if ($email) {
				$resultObj['callback-data'] = $email;
				$resultObj['no-message'] = true;
				$resultObj['callback'] = 'get-single-edit-email';
			} else
				$resultObj['error'] = "Invalid email.";
		} else
			$resultObj['error'] = "Unknown email.";
		break;

	case 'single-view':
		if (isset($_POST['id'])) {
			$email = $db->getSingleEmail($_POST['id']);
			if ($email) {
				$resultObj['callback-data'] = $email;
				$resultObj['no-message'] = true;
				$resultObj['callback'] = 'get-single-email';
			} else
				$resultObj['error'] = "Invalid email.";
		} else
			$resultObj['error'] = "Unknown email.";
		break;

	case 'single-send':
		if (isset($_POST['id'])) {
			$email = $db->getSingleEmail($_POST['id']);
			if ($email) {
				$resultObj['callback-data'] = $email;
				$resultObj['no-message'] = true;
				$resultObj['callback'] = 'get-single-send-email';
			} else if ($_POST['id'] == "blank") {
				$resultObj['callback'] = 'blank-send-email';
				$resultObj['no-message'] = true;
			} else
				$resultObj['error'] = "Invalid email.";
		} else
			$resultObj['error'] = "Unknown email.";
		break;

	case 'edit':
		if (Functions::validateArray($_POST, $rules, $filters, $resultObj['error'])) {
			if ($db->editEmail($_POST)) {
				$resultObj['success'] = "{$_POST['subject']} was edited successfully.";
				$resultObj['callback'] = "reload-registrations";
				$resultObj['reset'] = true;
				//Tokenizer::delete(array('post-action-email', 'post-case-edit'));
			} else
				if (Config::DEBUG_CORE)
					$resultObj['error'] = Config::DEFAULT_DB_ERROR;
		}
		break;

	case "delete":
		$resultObj['callback'] = "swal";
		$resultObj['no-message'] = true;
		if (isset($_POST['id'])) {
			$email = $db->getSingleEmail($_POST['id']);
			if ($email) {
				$resultObj['callback-data'] = array("title" => "Deleted!",
					"message" => $email['email_subject'] . " has been deleted.",
					"type" => "success",
					"next-action" => "reload-registrations");
				//Tokenizer::delete(array('post-action-email', 'post-case-delete'));
				$resultObj['delete'] = $db->deleteEmail($_POST['id']);
			} else
				$resultObj['error'] = "Invalid email.";
		} else
			$resultObj['error'] = "Unknown email.";

		if ($resultObj['error'] != "-1") {
			$resultObj['callback-data'] = array("message" => $resultObj['error'] . " Updating list...",
				"type" => "error",
				"next-action" => "reload-registrations");
		}
		break;

	case 'send':
		if (true) { //Validation with gump
			#var_dump($_POST);die();
			if ($_POST['email_id'] == "blank") {
				$email['email_subject'] = $_POST['subject'];
				$email['email_message'] = $_POST['message'];
			} else {
				$email = $db->getSingleEmail($_POST['email_id']);
			}

			if ($email) {
				$from = Config::DEFAULT_NOREPLY_EMAIL;
				$to = "aymandiab013@gmail.com";
				$carbon_copies = isset($_POST['carbon_copies']) ? $_POST['carbon_copies'] : null;
				$attachments = isset($_POST['attachment']) ? $_POST['attachment'] : null;
				$message = $email['email_message'];

				if (Functions::sendEmail($from, $to, $email['email_subject'], $message, implode(", ", $carbon_copies), $attachments)) {
					foreach (explode(",", $_POST['ids']) as $receiverId)
						ConvosDBManager::please()->addConvo(array(
							"user_id" => $_SESSION['user']['user_id'],
							"receiver_id" => $receiverId,
							"subject" => $email['email_subject'],
							"message" => $email['email_message']
						));

					$resultObj['success'] = "Email sent successfully";
					$resultObj['callback'] = "send-email";
					if (isset($attachments)) {
						foreach ($attachments as $attachment) {
							unlink($attachment['path']);
						}
					}
				} else {
					$resultObj['error'] = "Email could not be sent.";
				}
			} else {
				$resultObj['error'] = "Email doen't exist.";
			}
		}
		break;
}