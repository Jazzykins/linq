<?php
	$db = new ActivitySectorDBManager();

	switch ($_POST['case']) {
		case "single":
		 	$resultObj['no-message'] = true;
			$resultObj['email'] = $db->getSingleEmail($_POST['id']);
		break;
		case "add":
			if(Functions::validateArray($_POST, array('name' => 'required|max_len,100'), array('name' => 'trim|sanitize_string'), $resultObj['error'])){
				if(!$db->addSector($_POST))
					$resultObj['error'] = "Oupps our system had a little hiccup, please try again!";
				else {
					$resultObj['success'] = $_POST['name'] . " added successfully";
					$resultObj['callback'] = "add-activity-sector";
					Tokenizer::delete(array('post-action-sector','post-action-sector-add'));
				}
			}
		break;
		case "delete":
			$sector = $db->getSingleSector($_POST['sector_id']);
			if($sector){
				$resultObj['no-message'] = true;
				$resultObj['name'] = $sector['name'];
				$resultObj['delete'] = $db->deleteSector($_POST['sector_id']);
				$resultObj['success'] = $sector['name'] . " deleted successfully.";
			} else {
				$resultObj['error'] = "You do not have the permission to perform this action.";
			}
		break;
		case "edit":
			//GUMP Validation to be here
			if(Functions::validateArray($_POST, array('name' => 'required|max_len,100'), array('name' => 'trim|sanitize_string'), $resultObj['error'])){
				if(!$db->editSector($_POST))
					$resultObj['error'] = "Oupps our system had a little hiccup, please try again!";
				else
					$resultObj['success'] = $_POST['name'] . " edited successfully.";
			}

			if($resultObj['error'] == "-1"){
				$resultObj['callback'] = "edit-activity-sector";
				Tokenizer::delete(array('post-action-sector','post-action-sector-edit'));
			}
		break;
	}
?>
