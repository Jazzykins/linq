<?php 

	/*
		test usage of GUMP class for validation
		made for post test tool
	 */

	$rules = array(
		'action'		  => 'required|max_len,150|min_len,4',
		'case'		 	  => 'max_len,3',
		'version'		  => 'required|max_len,150|min_len,4'
	);
	$filters = array(
		'action' 	      => 'trim|sanitize_string',
		'case' 	     	  => 'trim|sanitize_string',
		'version' 	      => 'trim|sanitize_string'
	);

	switch ($_POST['case']) {
		default:
			if(Functions::validateArray($_POST, $rules, $filters, $resultObj['error']))
				$resultObj['success'] = "Validated Data!";
			break;
	}
			

	