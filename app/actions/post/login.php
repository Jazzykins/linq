<?php
	$db = new UserDBManager();

	if(!isset($_POST['login']) || Functions::isEmpty($_POST['login']))
		$resultObj['error'] = "Please enter your username or email to continue.";
	else if(!isset($_POST['password']) || Functions::isEmpty($_POST['password'], false))
		$resultObj['error'] = "Your password cannot be empty.";
	else {
		$user = $db->signIn($_POST['login'], $_POST['password']);

		if(!$user)
			$resultObj['error'] = "Your login credentials are invalid.";
		else if($user['status'] == User::BANNED)
			$resultObj['error'] = "Your account has been suspended.";
		else{
			if(isset($_POST['remember_me'])){
				$key = "";
				do {
					$key = Tokenizer::generateString(rand(15, 30));
				} while($db->getUserKey($key, "AUTO_LOGIN"));
				$db->generateUserKey($user['user_id'], $key, 'AUTO_LOGIN');
				setcookie("remember_me", $key, time() + 7 * 24 * 60 * 60, "/");
			}

			$_SESSION['user'] = $user;
			$db->updateLastLogin($_SESSION['user']['user_id']);
			$resultObj['no-reset'] = true;
			$resultObj['remove-disable'] = false;
			$resultObj['success'] = "Welcome back " . $user['username'] . "...";
			$resultObj['destination'] = isset($_SESSION['destination']) ? $_SESSION['destination'] : "main";
			Tokenizer::delete(array('post-action-login', 'post-version-login'));
		}
	}
?>
