<?php
	$db = new UserDBManager();

	switch ($_POST['case']) {
		case "single":
		 	$resultObj['no-message'] = true;
			$resultObj['user'] = $db->getSingleUser($_POST['id']);
		break;

		case "add":
			if(User::isValidFields($_POST,
				array('name', 'username', 'email', 'password'),
				array('fullname', 'username', 'email', 'password'),
				$resultObj['error'])){

				if(!$db->addUser($_POST))
					$resultObj['error'] = "Oupps our system had a little hiccup, please try again!";
				else{
					$resultObj['success'] = $_POST['name'] . " added successfully";
					$resultObj['callback'] = "add-user";
					if(isset($_POST['emailUser'])){
						$msg = "New user registration on " . Config::WEBSITE_TITLE . "<br><br>";
						$msg .= "Username " . $_POST['username'] . "<br>";
						$msg .= "Password " . $_POST['password'] . "<br><br>";
						$msg .= "Please follow this link in order to login and change your password: <a href='".Config::WEBSITE_URL."/login'link</a>";

						Functions::sendEmail(Config::DEFAULT_NOREPLY_EMAIL, $_POST['email'], "New Registration", $msg);
					}
					Tokenizer::delete(array('post-action-user','post-action-user-add'));
				}
			}
		break;

		case "create-password":
			if(User::isValidField($_POST, 'password', 'new-password', $resultObj['error'])){
				if(!isset($_POST['cpassword']))
					$resultObj['error'] = "Please confirm your password.";
				else if($_POST['password'] != $_POST['cpassword'])
					$resultObj['error'] = "Please make sure both passwords are the same.";
				else{
					$db->updatePassword($_SESSION['user']['user_id'], $_POST['password']);
					$resultObj['destination'] = "main";
					$resultObj['after-success'] = "Your password has been updated.";
					$resultObj['delay'] = 0;
					Tokenizer::delete(array('post-action-user','post-version-create-password', 'post-case-create-password'));
				}
			}
			break;

		case "delete":
			$user = $db->getUser($_POST['user_id']);
			if($user && $_SESSION['user']['level'] > $user['level']){
				$resultObj['no-message'] = true;
				$resultObj['username'] = $user['username'];
				$resultObj['delete'] = $db->deleteUser($_POST['user_id']);
			}else
				$resultObj['error'] = "You do not have the permission to perform this action.";
		break;

		case "edit":
			if(isset($_POST['deleteUser'])){
				if(!$db->deleteUser($_POST['user_id']))
					$resultObj['error'] = "Oupps our system had a little hiccup, please try again!";
				else
					$resultObj['success'] = $_POST['name'] . " deleted successfully.";
			}else if(User::isValidFields($_POST,
				array('name', 'username', 'email'),
				array('fullname', 'username', 'email'),
				$resultObj['error'])){

				if(!$db->editUser($_POST))
					$resultObj['error'] = "Oupps our system had a little hiccup, please try again!";
				else
					$resultObj['success'] = $_POST['name'] . " edited successfully.";
			}

			if($resultObj['error'] == "-1"){
				$resultObj['callback'] = "edit-user";
				Tokenizer::delete(array('post-action-user','post-action-user-edit'));
			}
		break;

		case "forgot-password":
			if(User::isValidField($_POST, 'email', 'email', $resultObj['error'])){
				$user = $db->getUserByEmail($_POST['email']);
				if($user){
					$key = $db->getUserKeyByUserId($user['user_id'], "RESET_PASSWORD");
					if(!$key){
						do{
							$key = Tokenizer::generateString(rand(30, 50));
						}while($db->getUserKey($key, "RESET_PASSWORD"));
						$db->generateUserKey($user['user_id'], $key, 'RESET_PASSWORD');
					}else{
						$key = $key['value'];
					}

					$msg = "Hello {$user['name']},
					<br>
					<br>Here is your password reset link: " . Config::WEBSITE_URL . "/reset-password/$key
					<br>
					<br>" . (Config::PASSWORD_RESET_KEY_DURATION != 0 ? "The password reset key will be valid for the next " . Config::PASSWORD_RESET_KEY_DURATION . " hours." : "") . "
					<br>If you did not request the password reset, please ignore this message.";
					Functions::sendEmail("donotreply@bbtutorials.com", $_POST['email'], "Reset Password", $msg);
					$resultObj['delay'] = 0;
					$resultObj['destination'] = "login";
					$resultObj['after-success'] = "An email with the instructions has been sent. Please look in your spam folder.";
					Tokenizer::delete('post-action-forgot-password', 'post-case-forgot-password');
				}else
					$resultObj['error'] = "Sorry, we do not have any account associated with <b>{$_POST['email']}</b>.";
			}
			break;
	}
?>