<?php
	$db = new ContactDBManager();

	$rules = array(
		'name' => "required|max_len,50|valid_name",
		'email' => 'required|max_len,100|valid_email',
		'comment' => 'required'
	);

	$filters = array(
		'name' => 'trim|sanitize_string',
		'email' => 'trim|sanitize_email',
		'comment' => 'trim'
	);

	switch ($_POST['case']) {
		case "single":
		 	$resultObj['no-message'] = true;
			$resultObj['contact'] = $db->getSingleContact($_POST['id']);
		break;
		case "add":
			if(Functions::validateArray($_POST, $rules, $filters, $resultObj['error'])){ //Validation with GUMP to be added here:
				if(!$db->addContact($_POST))
					$resultObj['error'] = "Oupps our system had a little hiccup, please try again!";
				else{
					$resultObj['success'] = $_POST['name'] . " added successfully";
					$resultObj['callback'] = "add-contact";
					Tokenizer::delete(array('post-action-contact','post-action-contact-add'));
				}
			}
		break;
		case "delete":
			$contact = $db->getSingleContact($_POST['contact_id']);
			if($contact){
				$resultObj['no-message'] = true;
				$resultObj['name'] = $contact['name'];
				$resultObj['delete'] = $db->deleteContact($_POST['contact_id']);
			} else
				$resultObj['error'] = "You do not have the permission to perform this action.";
		break;
		case "edit":
			if(true){ //Validation with GUMP to be added here:
				if(!$db->editContact($_POST))
					$resultObj['error'] = "Oupps our system had a little hiccup, please try again!";
				else
					$resultObj['success'] = $_POST['name'] . " edited successfully.";
			}
			if($resultObj['error'] == "-1"){
				$resultObj['callback'] = "edit-contact";
				Tokenizer::delete(array('post-action-contact','post-action-contact-edit'));
			}
		break;
	}
?>
