
<?php
	$db = new TaskDBManager();

	$rules = array(
		'title' => 'required|max_len,250',
		'client' => 'required',
		'assignment' => 'required',
		'due_date' => 'required',
		'priority' => 'required|integer'
	);

	$filter = array(
		'title' => 'trim|sanitize_string',
		'client' => 'trim|sanitize_string',
		'assignment' => 'trim|sanitize_string',
		'due_date' => 'trim|sanitize_string',
		'priority' => 'trim|sanitize_string|whole_number'
	);

	switch ($_POST['case']) {
		case "single":
			$task = $db->getSingleTask($_POST['id']);

			if($task['assignment'] == $_SESSION['user']['user_id'])
				$task['assignment'] = "self";

			$resultObj['no-message'] = true;
			$resultObj['task'] = $task;
			break;
		case "add":
			if(Functions::validateArray($_POST, $rules, $filter, $resultObj['error'])){
				$_POST['creator'] = isset($_POST['creator']) && $_POST['creator'] == "user" ? $_SESSION['user']['user_id'] : 0;
				$_POST['assignment'] = $_POST['assignment'] == "self" ? $_SESSION['user']['user_id'] : $_POST['assignment'];
				$_POST['client'] = $_POST['client'] == "n/a" ? 0 : $_POST['client'];
				if(!$db->addTask($_POST))
					$resultObj['error'] = "Oupps our system had a little hiccup, please try again!";
				else {
					$resultObj['success'] = $_POST['title'] . " added successfully";
					$resultObj['callback'] = "add-task";
					Tokenizer::delete(array('post-action-task','post-action-task-add'));
				}
			}
			break;
		case "edit":
			if(Functions::validateArray($_POST, $rules, $filter, $resultObj['error'])){
				//Use IDOfuscate...
				$_POST['creator'] = isset($_POST['creator']) && $_POST['creator'] == "user" ? $_SESSION['user']['user_id'] : 0;
				$_POST['assignment'] = $_POST['assignment'] == "self" ? $_SESSION['user']['user_id'] : $_POST['assignment'];
				if(!$db->editTask($_POST))
					$resultObj['error'] = "Oupps our system had a little hiccup, please try again!";
				else
					$resultObj['success'] = $_POST['title'] . " edited successfully.";
			}
			if($resultObj['error'] == "-1"){
				$resultObj['callback'] = "edit-task";
				Tokenizer::delete(array('post-action-task','post-action-task-edit'));
			}
			break;
		case "delete":
			$task = $db->getSingleTask($_POST['task_id']);
			if($task){
				$resultObj['no-message'] = true;
				$resultObj['title'] = $task['title'];
				$resultObj['delete'] = $db->deleteTask($_POST['task_id']);
				$resultObj['success'] = $task['title'] . " deleted successfully.";
			} else {
				$resultObj['error'] = "You do not have the permission to perform this action.";
			}
			break;
	}
?>
