<?php
	$db = new ApplicationDBManager();

    $rules = array(
    	'nom' => 'required|max_len,50|min_len,2',
    	'prenom' => 'required|max_len,50|min_len,2',
    	'tel' => 'required|phone_number',
    	'adresse' => 'required|max_len,250|min_len,10',
    	'ville' => 'required|max_len,100|min_len,4',
    	'cPostal' => 'required|max_len,25|min_len,6',
    	'courriel' => 'required|valid_email',
    	'dateProjet' => 'required|max_len,100|min_len,4',
    	'details' => 'required|max_len,1000|min_len,5',
    	'addfile' => 'extension,png;jpg',
    );

    $filters = array(
    	'nom' => 'trim|sanitize_string',
    	'prenom' => 'trim|sanitize_string',
    	'tel' => 'trim|sanitize_string',
    	'adresse' => 'trim|sanitize_string',
    	'ville' => 'trim|sanitize_string',
    	'cPostal' => 'trim|sanitize_string',
    	'courriel' => 'trim|sanitize_email',
    	'dateProjet' => 'trim|sanitize_string',
    	'details' => 'trim|sanitize_string',
    );

    $fileTypes = array(
    	"jpeg", "jpg", "png", "gif", "pdf", "docx", "txt", "rtf", "doc"
    );

	switch ($_POST['case']) {
		case "single":
		 	$resultObj['no-message'] = true;
			$resultObj['contact'] = $db->getSingleApplication($_POST['id']);
	        break;
		case "add":
            if (Functions::validateArray($_POST, $rules, $filters, $resultObj['error'])) {
                if (Functions::validateFiles($_POST['addfile'], $_FILES['addfile'], $resultObj['error'], false, $fileTypes)) {

                    $_POST['attachment'] = isset($_POST['attachment']) ? $_POST['attachment'] : NULL;
                    $_POST['conjoint'] = isset($_POST['conjoint']) ? $_POST['conjoint'] : NULL;

                    if($_POST['faillite'] == "oui")
                        $_POST['dateFaillite'] = isset($_POST['dateFaillite']) ? $_POST['dateFaillite'] : "Oui";
                    else if($_POST['faillite'] == "non")
                        $_POST['dateFaillite'] = "Non";

                    switch($_POST['etatcredit']){
                        case 1:
                            $_POST['etatcredit'] = "Très Faible";
                            break;
                        case 2:
                            $_POST['etatcredit'] = 'Faible';
                            break;
                        case 3:
                            $_POST['etatcredit'] = 'Moyen';
                            break;
                        case 4:
                            $_POST['etatcredit'] = 'Bon';
                            break;
                        case 5:
                            $_POST['etatcredit'] = 'Excellent';
                            break;
                    }

                    if(!$db->addApplication($_POST)){
                        $resultObj['error'] = "Oupps our system had a little hiccup, please try again!";
                    } else {
                        $resultObj['success'] = "Application for " . $_POST['prenom'] . " " . $_POST['nom'] . " added successfully";
                        $resultObj['callback'] = "add-application";
                        Tokenizer::delete(array('post-action-application','post-action-application-add'));
                    }
                }
            }
	        break;
		case "edit":
            if (Functions::validateArray($_POST, $rules, $filters, $resultObj['error'])) {
                if (Functions::validateFiles($_POST['addfile'], $_FILES['addfile'], $resultObj['error'], false, $fileTypes)) {

                    $_POST['attachment'] = isset($_POST['attachment']) ? $_POST['attachment'] : NULL;
                    $_POST['conjoint'] = isset($_POST['conjoint']) ? $_POST['conjoint'] : NULL;

                    if($_POST['faillite'] == "oui")
                        $_POST['dateFaillite'] = isset($_POST['dateFaillite']) ? $_POST['dateFaillite'] : "Oui";
                    else if($_POST['faillite'] == "non")
                        $_POST['dateFaillite'] = "Non";

                    switch($_POST['etatcredit']){
                        case 1:
                            $_POST['etatcredit'] = "Très Faible";
                            break;
                        case 2:
                            $_POST['etatcredit'] = 'Faible';
                            break;
                        case 3:
                            $_POST['etatcredit'] = 'Moyen';
                            break;
                        case 4:
                            $_POST['etatcredit'] = 'Bon';
                            break;
                        case 5:
                            $_POST['etatcredit'] = 'Excellent';
                            break;
                    }

                    if(!$db->editApplication($_POST)){
                        $resultObj['error'] = "Oupps our system had a little hiccup, please try again!";
                    } else {
                        $resultObj['success'] = "Application for " . $_POST['prenom'] . " " . $_POST['nom'] . " edited successfully";
                        $resultObj['application_id'] = $_POST['application_id'];
                        $resultObj['callback'] = "edit-application";
                        Tokenizer::delete(array('post-action-application','post-action-application-edit'));
                    }
                }
            }
	        break;
        case "delete":
            $contact = $db->getSingleContact($_POST['contact_id']);
            if($contact){
                $resultObj['no-message'] = true;
                $resultObj['name'] = $contact['name'];
                $resultObj['delete'] = $db->deleteContact($_POST['contact_id']);
            } else
                $resultObj['error'] = "You do not have the permission to perform this action.";
            break;
        case 'add-note':
            if(Functions::validateArray($_POST,
                array('content' => 'required|max_len,250'),
                array('content' => 'trim|sanitize_string'),
                $resultObj['error'])){
                $_POST['application_id'] = IDObfuscator::decode($_POST['application_id']);
                if(!$db->attachNote($_POST)){
                    $resultObj['error'] = "Oupps our system had a little hiccup, please try again!";
                } else {
                    $resultObj['success'] = "Note added successfully";
                    $resultObj['callback'] = "add-note";
                    $resultObj['callback-data'] = $_POST['application_id'];
                    Tokenizer::delete(array('post-action-application','post-action-application-add-note'));
                }
            }
            break;
        case 'delete-note':
            $resultObj['no-message'] = true;
            $resultObj['delete'] = $db->detachNote($_POST['note_id']);
            $resultObj['application_id'] = IDObfuscator::decode($_POST['application_id']);
            break;
        case 'add-document':
            if(true){ //Gump validation
                if(Functions::validateFiles(
                    $_POST['document_path'],
                    $_FILES['document'],
                    $resultObj['error'],
                    array("image/jpeg", "image/jpg", "image/png", "image/gif", "application/pdf", "application/doc", "application/docx", "text/plain", "application/rtf")    #File types
                )){
                    $_POST['application_related'] = IDObfuscator::decode($_POST['application_related']);
                    if(!$db->attachDocument($_POST)){
                        $resultObj['error'] = "Oupps our system had a little hiccup, please try again!";
                    } else {
                        $resultObj['success'] = "Document added successfully";
                        $resultObj['callback'] = "add-document";
                        $resultObj['callback-data'] = $_POST['application_related'];
                        Tokenizer::delete(array('post-action-application','post-action-application-add-document'));
                    }
                }
            }
            break;
        case 'delete-document':
            $resultObj['no-message'] = true;
            $resultObj['delete'] = $db->detachDocument($_POST['document_id']);
            $resultObj['application_id'] = IDObfuscator::decode($_POST['application_related']);
            break;
	}
?>
