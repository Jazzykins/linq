<?php require_once('header.php'); ?>
<?php require_once('load/menu-top.php'); ?>
<?php require_once('load/menu-left.php'); ?>
<?php
if (file_exists("../head.php"))
    include("../head.php");

if (isset($_SESSION['user']) && $_SESSION['user']['level'] < 50)
    Functions::redirect("main");
?>
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div id="logs-wrapper">
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                    	<div class="panel-heading">
                    		<h2 class="pull-left">Registres</h2>
                    		<div class="clearfix"></div>
                    	</div>
                    	<div class="panel-body">
                    		<div class="p-20">
                    			<table class="table table-striped m-0" id="datatable-editable">
                    				<thead>
                    				<tr>
                                        <th width="15%">Date</th>
                                        <th width="40%">Description</th>
                    					<th width="20%">Related Section</th>
                                        <th width="25%">Username</th>
                    				</tr>
                    				</thead>
                    				<tbody>
                    				<?php
                    				$total = 0;
                    				foreach ($db->getAllLogs() as $log) {
                    						$total++; ?>
                						<tr>
                                            <td><?php echo Functions::userFriendlyDate($log['action_date'])?></td>
                							<td><?php echo $log['action_description']?></td>
                							<td><?php echo $log['related_section']?></td>
                                            <td><?php echo $log['user_username']?></td>
                						</tr>
                						<?php
                    				}
                    				if ($total == 0)
                    					echo "<tr><td colspan='6' class='text-center'><i>Oupps, no logs found. Are you sure you have any?</i></td></tr>"
                    				?>
                    				</tbody>
                    			</table>
                    		</div>
                    		<p class="text-muted font-13">Total of <span class="users-count"><?php echo $total; ?>
                    				Log<?php echo $total > 1 ? 's' : '' ?></span></p>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('load/menu-right.php'); ?>
<?php require_once('footer.php'); ?>
<?php require_once('foot.php'); ?>
