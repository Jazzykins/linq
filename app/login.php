<?php require_once('head.php'); ?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="assets/img/logo-fav.png">
	<title>Beagle</title>
	<link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/lib/toastr/toastr.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="assets/css/style.css" type="text/css"/>

	<script src="assets/lib/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
	<script src="assets/lib/jquery-cookie/jquery.cookie.js"></script>
</head>
<body class="be-splash-screen">
<?php
if (User::isLoggedIn()) {
	Functions::generateErrorMessage("You're already logged in.");
	Functions::redirect("main");
}
?>

<div class="be-wrapper be-login">
	<div class="be-content">
		<div class="main-content container-fluid">
			<div class="splash-container">
				<div class="panel panel-default panel-border-color panel-border-color-danger">
					<div class="panel-heading"><img src="assets/img/logo-xx.png" alt="logo" width="102" height="27"
					                                class="logo-img"><span class="splash-description">Please enter your user information.</span>
					</div>
					<div class="panel-body">
						<form data-parsley-validate="" novalidate>
							<div class="login-form">
								<?php require_once('load/misc/ajax-loader.php'); ?>
								<input type="hidden" name="action"
								       value="<?php echo Tokenizer::add('post-action-login', 20, 'login'); ?>">
								<div class="form-group ">
									<input class="form-control" type="text" required="" placeholder="Username"
									       name="login">
								</div>

								<div class="form-group">
									<input class="form-control" type="password" required="" placeholder="Password"
									       name="password" autocomplete="new-password">
								</div>

								<div class="form-group row login-tools">
									<div class="col-xs-6 login-remember">
										<div class="be-checkbox">
											<input type="checkbox" id="remember" name="remember_me">
											<label for="remember">Remember Me</label>
										</div>
									</div>
									<div class="col-xs-6 login-forgot-password"><a href="forgot-password">Forgot
											Password?</a></div>
								</div>

								<div class="form-group row login-submit">
									<div class="col-xs-6">
										<button data-dismiss="modal" type="submit" class="btn btn-default btn-xl">
											Register
										</button>
									</div>
									<div class="col-xs-6">
										<button data-dismiss="modal" type="submit" class="btn btn-danger btn-xl">Sign
											in
										</button>
									</div>
								</div>
							</div>
						</form>
						<p class="text-center">Copyright &copy; <a href="http://w3consultation.com" target="_blank">W3Consultation</a> <?php echo date("Y") ?>.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php require_once('footer.php'); ?>
	<?php require_once('foot.php'); ?>
