<?php require_once('header.php'); ?>
<?php require_once('load/menu-top.php'); ?>
<?php require_once('load/menu-left.php'); ?>

	<div class="be-content">
		<div class="main-content container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-border-color panel-border-color-dark">
						<div class="panel-heading">This is just a test tool to test the core action with specific
							version
						</div>
						<div class="panel-body">
							<form onsubmit="return validate(this);">
								<div class="form-group">
									<label for="action">Action</label>
									<input type="text" name="action" id="action"
									       value="<?php echo Tokenizer::add('post-action-login', 20, 'login'); ?>"
									       class="form-control">
								</div>
								<div class="form-group">
									<label for="version">Version</label>
									<input type="text" name="version" id="version"
									       value="<?php echo Tokenizer::add('post-action-login-version', 20, '1.0.0'); ?>"
									       class="form-control">
								</div>
								<div class="form-group text-right">
									<button type="submit" class="btn btn-success btn-xl">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php require_once('load/menu-right.php'); ?>
<?php require_once('footer.php'); ?>
<?php require_once('foot.php'); ?>