<?php require_once('header.php'); ?>
<?php NotificationDBManager::please()->markAllNotificationSeen(); ?>
<?php require_once('load/menu-top.php'); ?>
<?php require_once('load/menu-left.php'); ?>
<?php
if (isset($_SESSION['user']) && $_SESSION['user']['level'] < 50)
Functions::redirect("main");
?>
<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Timeline</h2>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div id="notifications-wrapper">
                    <ul class="timeline">
                        <?php
                        foreach(NotificationDBManager::please()->getAllNotifications() as $notification){ ?>
                            <li class="timeline-item">
                                <div class="timeline-date">
                                    <span><?php echo date("F d, Y" , strtotime($notification['creation_date']))?></span>
                                </div>
                                <div class="timeline-content">
                                    <div class="timeline-avatar">
                                        <img src="<?php echo $notification['icon']?>" alt="<?php echo $notification['title']?>">
                                    </div>
                                    <div class="timeline-header">
                                        <span class="timeline-time">
                                            <?php echo date("h:i a", strtotime($notification['creation_date'])) ?>
                                        </span>
                                        <span class="timeline-autor">
                                            <?php echo $notification['title'] ?>
                                        </span>
                                        <span class="timeline-summary">
                                            <p><?php echo $notification['description'] ?></p>
                                        </span>
                                    </div>
                                </div>
                            </li>
                            <?php
                        } ?>
                        <li class="timeline-item timeline-loadmore"><a href="#" class="load-more-btn">Load more</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('load/menu-right.php'); ?>
<?php require_once('footer.php'); ?>
<?php require_once('foot.php'); ?>
